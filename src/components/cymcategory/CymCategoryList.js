/**
 * Created by jedachas on 3/9/17.
 */
 import React, {Component} from 'react';
 import ListTable from '../common/table/ListTableComponent';
 import GridRow from './CymCategoryGridRow';

class CymCategoryList extends Component {
  handleGetData = (params) => {
    this.props.actions.getCymCategorys(params);
  }

  render(){
    let {cymcategorys} = this.props.data;
    const headers = [
      { title: '', value: ''},
      { title: 'Name', value: 'name'},
      { title: 'Order', value: 'order'},
      { title: 'Status', value: 'status'}
    ];

    var rows = [];
    if (cymcategorys.records.length > 0) {
      cymcategorys.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}/>);
      })
    }

    return(
      <div className='content-container'>

        <ListTable
          headers={headers}
          data={this.props.data.cymcategorys}
          onGetData={this.handleGetData}>
            {rows}
        </ListTable>
      </div>
    );
  }

}

export default CymCategoryList;
