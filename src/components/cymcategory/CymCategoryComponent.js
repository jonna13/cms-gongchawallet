'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import Config from '../../config/base';
import {AddButton} from '../common/buttons';
import {SearchBar,CheckStatus,PageSize} from '../common/widgets';

import CymCategoryList from './CymCategoryList';

class CymCategoryComponent extends React.Component {

  handleAdd = () => {
    const {router} = this.props;
    router.push('/cymcategory_add');
  }

  handleEdit = (item) => {
    const {router} = this.props;
    router.push('/cymcategory/'+item.cymCategoryID);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.cymcategorys;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.cymcategorys;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (text) => {
    let {pageinfo} = this.props.data.cymcategorys;
    pageinfo.searchFilter = text;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.cymcategorys;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.cymcategorys;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus
    }
    console.info('CymCategoryComponent: param', params);
    actions.getCymCategorys(params);
  }

  render() {
    const {pageinfo} = this.props.data.cymcategorys;
    const {data} = this.props;
    console.log('cymCategory', data);
    return (
      <div>
        <h2 className='content-heading'>CymCategory</h2>
        <h5 className='content-record-label'>Records: <b>{ pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <AddButton
            handleOpen={this.handleAdd}/>
        <CheckStatus
            pageinfo={data.cymcategorys.pageinfo}
            handleChange={this.handleStatusChange}/>
        <PageSize
            pageinfo={data.cymcategorys.pageinfo}
            handleChange={this.handlePageSize}/>
        <SearchBar
            pageinfo={data.cymcategorys.pageinfo}
            handleChange={this.handleSearchChange}
            handleClose={this.handleSearchClose}/>

        { /* List */ }
        <CymCategoryList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit} />
      </div>
    );
  }
}

CymCategoryComponent.displayName = 'CymCategoryCymCategoryComponent';

// Uncomment properties you need
// CymCategoryComponent.propTypes = {};
// CymCategoryComponent.defaultProps = {};

export default CymCategoryComponent;
