/**
 * Created by jedachas on 3/9/17.
 */
 import React, {Component} from 'react';
 import ListTable from '../common/table/ListTableComponent';
 import Config from '../../config/base';

 import GridRow from './CategoryGridRow';

class CategoryList extends Component {
  handleGetData = (params) => {
    this.props.actions.getCategorys(params);
  }

  render(){
    let {categorys} = this.props.data;
    const headers = [
      { title: '', value: ''},
      { title: 'Name', value: 'name'},
      { title: 'Order', value: 'order'},
      { title: 'Status', value: 'status'}
    ];

    var rows = [];
    if (categorys.records.length > 0) {
      categorys.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}
          module={Config.IMAGE.CATEGORY}/>);
      });
    }

    return(
      <div className='content-container'>

        <ListTable
          headers={headers}
          data={this.props.data.categorys}
          onGetData={this.handleGetData}>
            {rows}
        </ListTable>
      </div>
    );
  }

}

export default CategoryList;
