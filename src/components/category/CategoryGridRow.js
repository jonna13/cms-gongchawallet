import React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/content/create';
import Config from '../../config/base';
import AppStyles from '../../styles/Style';

class CategoryGridRow extends Component {
  render(){
    let item = this.props.items;

    return(
      <tr>
        <td>
          <IconButton tooltip="Click to edit item"
            touch={true}
            tooltipPosition="bottom-left"
            iconStyle={AppStyles.editIcon}
            onClick={()=> { this.props.onEditClick(item); }}>
              <EditIcon />
          </IconButton>
        </td>
        <td>
          {item.name}
        </td>
        <td>
          {item.order}
        </td>
        <td>
          <label className={ (item.status == 'active' ) ? 'label-active' : 'label-inactive'}>{item.status}</label>
        </td>
      </tr>
    );
  }
}

export default CategoryGridRow;
