/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton, AddButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';

import CategoryForm from './CategoryForm';

class CategoryEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      category: {
        name: '',
        description: '',
        status: ''
      }
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewCategory(this.props.params.id);
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.categoryState.selectedRecord)) {
        this.setState({
          category: nextProps.categoryState.selectedRecord
        });
      }
    }
  }

  // handle going back to Category
  handleReturn = () => {
    this.props.router.push('/category');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    if (this.validateInput(this.state.category)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_CATEGORY_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateCategory(this.state.category, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    console.info('handleAdd:state', this.state);
    if (this.validateInput(this.state.category)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_CATEGORY_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addCategory(this.state.category, this.props.router);
          }
      });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let category = this.state.category;
    category[field] = e.target.value;
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.category['status'] = e;
  }

  render(){
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Category' : 'Add Category'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <AddButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <CategoryForm
          data={this.props.categoryState}
          shouldEdit={ (this.props.params.id) ? true : false }
          onChange={this.handleData}
          onStatusChange={this.handleStatusChange}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    categoryState: state.categoryState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    categoryAction: require('../../actions/categoryAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.categoryAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryEditor);
