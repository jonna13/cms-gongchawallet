/**
 * Created by jedachas on 3/9/17.
 */
 import React, {Component} from 'react';
 import ListTable from '../common/table/ListTableComponent';
 import Config from '../../config/base';
 import GridRow from './SubcategoryGridRow';

class SubcategoryList extends Component {
  handleGetData = (params) => {
    this.props.actions.getSubcategorys(params);
  }

  render(){
    let {subcategorys} = this.props.data;
    const headers = [
      { title: '', value: ''},
      { title: 'Name', value: 'name'},
      { title: 'Quantity', value: 'quantity'},
      { title: 'Order', value: 'order'},
      { title: 'Category', value: 'categoryName'},
      { title: 'Status', value: 'status'}
    ];

    var rows = [];
    if (subcategorys.records.length > 0) {
      subcategorys.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}
          module={Config.IMAGE.SUBCATEGORY}/>);
      });
    }

    return(
      <div className='content-container'>

        <ListTable
          headers={headers}
          data={this.props.data.subcategorys}
          onGetData={this.handleGetData}>
            {rows}
        </ListTable>
      </div>
    );
  }

}

export default SubcategoryList;
