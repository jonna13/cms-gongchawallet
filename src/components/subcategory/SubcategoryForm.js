/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import TextField from 'material-ui/TextField';
import Config from '../../config/base';
import { DefaultTextArea } from '../common/fields';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import DatePicker from 'material-ui/DatePicker';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';


class SubcategoryForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      categoryID: '',
      shouldDisplay: false,
    };
  }

  componentWillMount(){
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      this.setState({
        categoryID: nextProps.data.selectedRecord.categoryID,
      });
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({
          shouldDisplay: true
        });
      }
    }
  }

  handleCategoryChange = (e, idx, categoryID) => {
    this.setState({categoryID});
    this.props.onCategoryChange(categoryID);
  };

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  render(){
    let categoryList = [];
    const {data} = this.props;

    if (data.categorys) {
      categoryList.push(<MenuItem key={0} value='' primaryText='Select Category' />);
      data.categorys.map( (val, idx) => {
        categoryList.push(<MenuItem key={(idx+1)} value={val.categoryID} primaryText={val.name} />);
      });
    }

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-container'>
                <div className='item-image-box'>
                  <ImageUpload image={null}
                    onImageChange={this.props.onImageChange}
                    image={(data.selectedRecord.image) ? data.selectedRecord.image : '' }
                    imageModule={this.props.imageModule}/>
                </div>
              </div>
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='pull-left content-field-holder'>
                <DropDownMenu
                  value={this.state.categoryID}
                  onChange={this.handleCategoryChange}
                  autoWidth={true}
                  openImmediately={(!this.props.shouldEdit) ? true: false}
                  className='dropdownButton-fixed-300'>
                  {categoryList}
                </DropDownMenu>
                <TextField
                  name="name"
                  hintText="Enter Name"
                  floatingLabelText="Name"
                  defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                  onChange={this.props.onChange}
                /><br />
                <DefaultTextArea
                  name="description"
                  hintText="Enter Heading"
                  floatingLabelText="Heading"
                  defaultValue={(data.selectedRecord.description) ? data.selectedRecord.description : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  regular={true}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  /><br />
                <TextField
                  name="quantity"
                  hintText="Enter Quantity"
                  floatingLabelText="Quantity"
                  defaultValue={(data.selectedRecord.quantity) ? data.selectedRecord.quantity : '' }
                  onChange={this.props.onChange}
                /><br />
                <TextField
                  name="order"
                  hintText="Enter Order"
                  floatingLabelText="Order"
                  defaultValue={(data.selectedRecord.order) ? data.selectedRecord.order : '' }
                  onChange={this.props.onChange}
                /><br />
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default SubcategoryForm;
