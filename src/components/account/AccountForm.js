/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import Config from '../../config/base';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import DatePicker from 'material-ui/DatePicker';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import { PasswordText } from '../common/fields';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';
import FlatButton from 'material-ui/FlatButton';
import {ListDrop, ListDropChips} from '../common/lists';
import {GeneratePassword} from '../common/Utility';
import jwtDecode from 'jwt-decode';


class AccountForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      shouldDisplay: false,
      passwordType: 'password',
      password: '',
      reenter: '',
      reportTabsDisplay: false,
      reportTabsRemove: false,
      cmsTabsDisplay: false,
      cmsTabsRemove: false,
      locationTabDisplay: false,
      selectedLocation: ''
    };
  }

  componentWillMount(){
    // for add
    if (!this.props.shouldEdit){
      this.setState({
        shouldDisplay: true
      });
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        console.info('selectedRecord', nextProps.data.selectedRecord);
        this.setState({
          shouldDisplay: true,
          reportTabsDisplay: (nextProps.data.selectedRecord.role != Config.CUSTOM_ROLE) ? false : true,
          cmsTabsDisplay: (nextProps.data.selectedRecord.role != Config.CUSTOM_ROLE) ? false : true,
          locationTabDisplay: (nextProps.data.selectedRecord.role == 'store-operation') ? true: false
        });
      }
    }
  }

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  handleRoleChange = (role) => {
    console.info('role', role);
    this.setState({
      reportTabsDisplay: (role != Config.CUSTOM_ROLE) ? false : true,
      cmsTabsDisplay: (role != Config.CUSTOM_ROLE) ? false : true,
      locationTabDisplay: (role == 'store-operation') ? true: false
    });
    this.props.onRoleChange(role);
  }

  handleGeneratePassword = () => {
    let newpass = GeneratePassword();
    this.setState({
      password: newpass,
      reenter: newpass
    });
    this.props.onGenerateValue('password', newpass);
    this.props.onGenerateValue('reenter', newpass);
  }

  handlePasswordChange = (e) => {
    this.setState({
      password: e.target.value
    });
    this.props.onGenerateValue('password', e.target.value);
  }

  handleRePasswordChange = (e) => {
    this.setState({
      reenter: e.target.value
    });
    this.props.onGenerateValue('reenter', e.target.value);
  }


  render(){
    const {data} = this.props;
    console.log('role', data);
    const role = jwtDecode(sessionStorage.getItem(Config.MERCHANT_NAME)).role;
    let roleArray = Config.ACCOUNT_ROLES;

    if (this.props.shouldEdit) {
      if (data.selectedRecord.role == Config.GODUSER) {
        roleArray = roleArray.concat(Config.DEVELOPER_ROLE);
      } else{
        roleArray = (role == Config.GODUSER) ? roleArray.concat(Config.DEVELOPER_ROLE) : Config.ACCOUNT_ROLES;
      }
    } else {
      roleArray = (role == Config.GODUSER) ? roleArray.concat(Config.DEVELOPER_ROLE) : Config.ACCOUNT_ROLES;
    }

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-container'>
                <div className='item-image-box'>
                  <ImageUpload image={null}
                    onImageChange={this.props.onImageChange}
                    image={(data.selectedRecord.profilePic) ? data.selectedRecord.profilePic : '' }
                    imageModule={this.props.imageModule}/>
                </div>
              </div>
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                <TextField
                  name="fullname"
                  hintText="Enter Fullname"
                  floatingLabelText="Fullname"
                  defaultValue={(data.selectedRecord.fullname) ? data.selectedRecord.fullname : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="username"
                  hintText="Enter Username"
                  floatingLabelText="Username"
                  defaultValue={(data.selectedRecord.username) ? data.selectedRecord.username : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <PasswordText
                  name="password"
                  value={this.state.password}
                  hintText="Enter Password"
                  floatingLabelText="Password"
                  onChange={this.handlePasswordChange}
                  className="textfield-regular"
                />
                <PasswordText
                  name="reenter"
                  value={this.state.reenter}
                  hintText="Re-Enter Password"
                  floatingLabelText="Password Again"
                  onChange={this.handleRePasswordChange}
                  className="textfield-regular"
                />
                <FlatButton label="Generate Password"
                  primary={true}
                  className='pull-right'
                  onClick={this.handleGeneratePassword} />
                <ListDrop
                  onChange={this.handleRoleChange}
                  placeholder="Select Role"
                  data={roleArray}
                  defaultValue={data.selectedRecord.role}
                  defaultKey="value"
                />
                <ListDropChips
                  show={this.state.reportTabsDisplay}
                  onChange={this.props.onReportTabChange}
                  placeholder="Select Report Tabs"
                  data={Config.REPORT_TABS}
                  remove={this.state.reportTabsRemove}
                  defaultValue={data.selectedRecord.reportTabs}
                  defaultKey="value"
                />
                <ListDropChips
                  show={this.state.cmsTabsDisplay}
                  onChange={this.props.onCmsTabChange}
                  placeholder="Select CMS Tabs"
                  data={Config.CMS_TABS}
                  remove={this.state.cmsTabsRemove}
                  defaultValue={data.selectedRecord.cmsTabs}
                  defaultKey="value"
                />
                <ListDrop
                  show={this.state.locationTabDisplay}
                  onChange={this.props.onLocationSelect}
                  placeholder="Select Branch"
                  data={this.props.data.locations}
                  defaultValue={data.selectedRecord.locID}
                  defaultKey="locID"
                />
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }
  }
}

export default AccountForm;
