'use strict';

import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Snackbar from 'material-ui/Snackbar';
import AppStyle from '../../styles/Style';

export class AlertDialog extends React.Component{

  handleClose = () => {
    let { dialogActions } = this.props;
    dialogActions.closeAlert();
  }

  render(){
    const actions = [
      <FlatButton
        label="Close"
        keyboardFocused={true}
        onTouchTap={this.handleClose}
      />
    ];

    return(
      <Dialog
        titleStyle={AppStyle.modalHeader}
        title={this.props.title}
        open={this.props.open}
        actions={actions}
        modal={true}
        repositionOnUpdate={true}
        className='modal-fullmode-xs'
        autoScrollBodyContent={false}>
          <span>
            {this.props.message}
          </span>
      </Dialog>
    );
  }
}

export class ConfirmDialog extends React.Component {
  handleClose = (e) => {
    if (this.props.dialogActions) {
      this.props.dialogActions.closeConfirm();
    }

    if (this.props.onClose) {
      this.props.onClose(false);
    }
  }

  handleConfirm = (e) => {
    if (this.props.dialogActions) {
      this.props.dialogActions.closeConfirm();
    }

    if (this.props.onClose) {
      this.props.onClose(true);
    }
  }

  render() {
    const actions = [
      <FlatButton
        label={ (this.props.confirmLabel) ? this.props.confirmLabel : 'Yes' }
        onTouchTap={this.handleConfirm}
        primary={true}
        style={AppStyle.SaveButtonBox}
      />,
      <FlatButton
        label={ (this.props.closeLabel) ? this.props.closeLabel : 'No' }
        onTouchTap={this.handleClose}
        keyboardFocused={true}
      />
    ]

    return (
      <Dialog
        title={this.props.title}
        actions={actions}
        modal={false}
        open={this.props.open}
        onRequestClose={this.handleClose}
        autoScrollBodyContent={false}>
        { this.props.message }
      </Dialog>
    );
  }
}


export class Notification extends React.Component{
  handleRequestClose = () => {
    if (this.props.dialogActions) {
      this.props.dialogActions.closeNotification();
    }
  }

  render(){
    return(
      <Snackbar
        open={this.props.open}
        message={this.props.message}
        autoHideDuration={this.props.delay}
        onRequestClose={this.handleRequestClose}
      />
    );
  }
}
