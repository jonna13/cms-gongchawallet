/**
 * Created by jedachas on 3/30/17.
 */
'use strict';

import React, {Component} from 'react';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import Eye from 'material-ui/svg-icons/image/remove-red-eye';

class PasswordText extends Component {
  state = {
    type: 'password'
  }

  handleType = () => {
    let type = this.state.type;
    this.setState({
      type: (type == 'password') ? 'text' : 'password'
    });
  }

  render(){
    return(
      <div>
        <TextField
          name={this.props.name}
          type={this.state.type}
          value={this.props.value}
          hintText={this.props.hintText}
          floatingLabelText={this.props.floatingLabelText}
          onChange={this.props.onChange}
        />
        <IconButton onClick={this.handleType}>
          <Eye />
        </IconButton>
      </div>
    );
  }
}

export default PasswordText;
