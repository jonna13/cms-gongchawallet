'use strict';

import React, {
  Component
} from 'react';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem'
import Chip from 'material-ui/Chip';
import Dialog from 'material-ui/Dialog';
import {InfoDialog} from '../dialogs';
import _ from 'lodash';

class ListDropMultiChips extends Component{
  constructor(props){
    super(props);
    this.state = {
        value: '',
        dataArray: []
      }
  }


  handleDataChange = (e, idx, val) => {
    console.info('handleDataChange', val);
    if (val) {

      if (val == 'all') {
        this.pushAll();
      }
      else{
        let name = this.props.data.filter( x =>{return x[this.props.defaultKey] == val})[0].name;
        let uniqueID = this.props.data.filter( x =>{return x[this.props.defaultKey] == val;})[0].id;
        let secondaryKey = this.props.data.filter( x =>{return x[this.props.defaultKey] == val;})
          [0][(this.props.SecondaryKey) ? this.props.SecondaryKey : this.props.defaultKey];
        if ( _.isEmpty(this.state.dataArray.filter(x=>{return x.key == val})) ) {
            let dataArray = this.state.dataArray;
            dataArray.push({key: val, name: name, id:uniqueID, secondaryKey: secondaryKey });
            this.setState({ dataArray: dataArray });
          }
      }
    }
    this.setState({value: val});
    this.props.onChange(this.state.dataArray);
  }

  pushAll = () => {
    let dataArray = this.state.dataArray;
    this.props.data.map( (val, idx) => {
      let secondaryKey = (this.props.SecondaryKey) ? val[this.props.SecondaryKey] : val[this.props.defaultKey];
      dataArray.push({key: val[this.props.defaultKey], name: val.name, id: val[this.props.defaultKey], secondaryKey: secondaryKey });
    });
    console.info('dataArray', dataArray);
  }

  componentWillReceiveProps(nextState){
    this.setState({value: ''});
    if (nextState.remove) {
      this.setState({dataArray: []});
    }
    if (!_.isEmpty(nextState.defaultValue) && !_.isEmpty(nextState.data)) {
      let val = nextState.defaultValue.split(",").map( v=> {
        let vx = nextState.data.filter( x => {
            return x[nextState.defaultKey] == v;
          })[0];
        return {key: vx.id, name: vx.name,
          id: vx.id, secondaryKey: vx[(this.props.SecondaryKey) ? this.props.SecondaryKey : this.props.defaultKey] };
      });
      this.setState({dataArray: val});
    }
  }

  handleChipRemove = (key) => {
    const chipToDelete = this.state.dataArray.map((chip) => chip.key).indexOf(key);
    this.state.dataArray.splice(chipToDelete, 1);
    this.setState({dataArray: this.state.dataArray});
    this.props.onChange(this.state.dataArray);
  }

  handleTouchTap = (data) => {
    this.child.fetchData(data.id);
  }

  renderChips(data) {
    return (
      <Chip
        className="chip-data"
        key={data.key}
        onTouchTap={ () => this.handleTouchTap(data)}
        onRequestDelete={() => this.handleChipRemove(data.key)}
      > {data.name} </Chip>
    );
  }

  render(){
    let list = [];
    if (this.props.data) {
      list.push(<MenuItem key={0} value='' primaryText={this.props.placeholder} />);
      this.props.data.map( (val, idx) => {
        list.push(<MenuItem key={(idx+1)} value={val[this.props.defaultKey]} primaryText={val.name} />);
      });
      list.push(<MenuItem key={this.props.data.length+1} value='all' primaryText='All' />);
    }

    return(
      <div className={(this.props.show) ? 'visible' : 'hide'}>
        <DropDownMenu value={this.state.value} onChange={this.handleDataChange}
          autoWidth={false} className='dropdownButton'>
          {list}
        </DropDownMenu>
        <div className="chip-holder"> {this.state.dataArray.map(this.renderChips, this)} </div>

        <InfoDialog
          onRef={ref => (this.child = ref)}
          identity={this.props.identity}/>
      </div>
    );
  }
}

export default ListDropMultiChips;
