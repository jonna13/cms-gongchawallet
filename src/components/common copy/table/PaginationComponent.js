import React, {Component} from 'react';
import {Pagination} from 'react-bootstrap';

class PaginationComponent extends Component {
    render(){
      let pageCount = this.props.Size;

      if (pageCount > 0) {
        return (
          <Pagination
            prev
            next
            first
            last
            ellipsis
            boundaryLinks
            items={pageCount}
            maxButtons={5}
            activePage={this.props.currentPage}
            onSelect={(eventKey, e) => {
              this.props.onPageChanged(eventKey, e);
            }} />
        );
      }
      else{
        return null;
      }
    }
}

export default PaginationComponent;
