'use strict';

import React, {
  Component,
  PropTypes
} from 'react';
import { Link } from 'react-router';

import Business from 'material-ui/svg-icons/communication/business';
import Chat from 'material-ui/svg-icons/communication/chat-bubble';
import Cart from 'material-ui/svg-icons/action/shopping-basket';
import Category from 'material-ui/svg-icons/action/group-work';
import Subcategory from 'material-ui/svg-icons/action/reorder';
import Store from 'material-ui/svg-icons/action/store';
import Star from 'material-ui/svg-icons/action/stars';
import Branch from 'material-ui/svg-icons/communication/location-on';
import Loyalty from 'material-ui/svg-icons/action/loyalty';
import New from 'material-ui/svg-icons/av/fiber-new';
import Device from 'material-ui/svg-icons/device/devices';
import Lock from 'material-ui/svg-icons/action/lock-outline';
import Info from 'material-ui/svg-icons/action/info';
import Setting from 'material-ui/svg-icons/action/settings';
import Level from 'material-ui/svg-icons/av/recent-actors';
import Person from 'material-ui/svg-icons/social/person';
import classNames from 'classnames';
import Config from '../../config/base';
import jwtDecode from 'jwt-decode';
import InfoComponent from '../common/InfoComponent';

import { Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import {blue500, red500, greenA200} from 'material-ui/styles/colors';

import FontIcon from 'material-ui/FontIcon';

import Header from './HeaderComponent';


class MobileLinkNav extends React.Component {

  state = {
    activeKey: 1,
    open: false,
  }

  handleSelect = (selectedKey) => {
    this.setState({activeKey: selectedKey});

    switch (selectedKey) {
      case 1:
        this.props.router.push('/');
        break;
      case 2:
        this.props.router.push('/push');
        break;
      case 3:
        this.props.router.push('/category');
        break;
      case 4:
        this.props.router.push('/product');
        break;
      case 5:
        this.props.router.push('/post');
        break;
      case 6:
        this.props.router.push('/setting/EARNGC23143DbVSs');
        break;
      case 7:
        this.props.router.push('/account');
        break;

      // case 3.1:
      //   this.props.router.push('/category');
      //   break;
      // case 3.2:
      //   this.props.router.push('/subcategory');
      //   break;
      // case 3.3:
      //   this.props.router.push('/product');
      //   break;
      // case 4:
      //   this.props.router.push('/flavor');
      //   break;
      // case 5:
      //   this.props.router.push('/addon');
      //   break;
      // case 6:
      //   this.props.router.push('/drink');
      //   break;
      // case 7:
      //   this.props.router.push('/dip');
      //   break;
      // case 8:
      //   this.props.router.push('/side');
      //   break;
      // case 9:
      //   this.props.router.push('/upgrade');
      //   break;
      default:
        return;

    }
  }

  handleRouteReload = (pathname) => {
    if (pathname == '/push') {
      this.setState({activeKey: 2});
    }
    if (pathname == '/category') {
      this.setState({activeKey: 3});
    }
    else if (pathname.includes('/product')) {
      this.setState({activeKey: 4});
    }
    else if (pathname.includes('/post')) {
      this.setState({activeKey: 5});
    }
    else if (pathname.includes('/setting/EARNGC23143DbVSs')) {
      this.setState({activeKey: 6});
    }
    else if (pathname.includes('/account')) {
      this.setState({activeKey: 7});
    }

    // else if (pathname.includes('/category')) {
    //   this.setState({activeKey: 3.1});
    // }
    // else if (pathname.includes('/subcategory')) {
    //   this.setState({activeKey: 3.2});
    // }
    // else if (pathname.includes('/product')) {
    //   this.setState({activeKey: 3.3});
    // }
    // else if (pathname.includes('/flavor')) {
    //   this.setState({activeKey: 4});
    // }
    // else if (pathname.includes('/addon')) {
    //   this.setState({activeKey: 5});
    // }
    // else if (pathname.includes('/drink')) {
    //   this.setState({activeKey: 6});
    // }
    // else if (pathname.includes('/dip')) {
    //   this.setState({activeKey: 7});
    // }
    // else if (pathname.includes('/side')) {
    //   this.setState({activeKey: 8});
    // }
    // else if (pathname.includes('/upgrade')) {
    //   this.setState({activeKey: 9});
    // }
    else if (pathname == '/') {
      this.setState({activeKey: 1});
    }
  }

  componentWillMount(){
    let {pathname} = this.props.router.location;
    this.handleRouteReload(pathname);
  }

  handleInfo = (event) => {
    this.setState({open: true});
  }

  handleClose = () => {
    this.setState({open:false});
  }

  render() {

    const role = jwtDecode(sessionStorage.getItem(Config.MERCHANT_NAME)).role;
    let superUserOnly = classNames({
      'hide': !Config.SUPERUSER.includes(role),
      'visible': Config.SUPERUSER.includes(role)
    });

    return (
      <div className="visible-xs">
        <Header router={this.props.router} onInfo={this.handleInfo}/>

        <InfoComponent
          open={this.state.open}
          onRequestClose={this.handleClose}/>
      </div>
    )
  }
}


export default MobileLinkNav;
