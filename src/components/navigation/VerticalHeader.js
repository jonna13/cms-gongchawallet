'use strict';

// import React from 'react';
import React, {
  Component,
  PropTypes
} from 'react';

let logo = require('../../images/logo.png');


class VerticalHeader extends React.Component {
  render() {
    return (
      <div>

        <div className='v-header-image'>
          <img src={logo}/>
        </div>
        <div>
          { this.props.children }
        </div>

      </div>
    )
  }
}

VerticalHeader.displayName = 'NavigationVerticalHeader';


export default VerticalHeader;
