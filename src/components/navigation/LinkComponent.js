'use strict';

import React, {
  Component,
  PropTypes
} from 'react';
import { Link } from 'react-router';

import Business from 'material-ui/svg-icons/communication/business';
import Chat from 'material-ui/svg-icons/communication/chat-bubble';
import Cart from 'material-ui/svg-icons/action/shopping-basket';
import Category from 'material-ui/svg-icons/action/group-work';
import Subcategory from 'material-ui/svg-icons/action/reorder';
import Assignment from 'material-ui/svg-icons/action/assignment';
import Branch from 'material-ui/svg-icons/communication/location-on';
import Lock from 'material-ui/svg-icons/action/lock-outline';
import Info from 'material-ui/svg-icons/action/info';
import Flavor from 'material-ui/svg-icons/image/grain';
import Account from 'material-ui/svg-icons/action/account-circle';
import Inventory from 'material-ui/svg-icons/action/description';
import CYMCategory from 'material-ui/svg-icons/action/picture-in-picture';
import CYM from 'material-ui/svg-icons/action/picture-in-picture-alt';
import Package from 'material-ui/svg-icons/action/work';
import Drink from 'material-ui/svg-icons/maps/local-drink';
import Offer from 'material-ui/svg-icons/maps/local-offer';
import New from 'material-ui/svg-icons/av/fiber-new';
import Setting from 'material-ui/svg-icons/action/settings';
import Loyalty from 'material-ui/svg-icons/action/loyalty';
import City from 'material-ui/svg-icons/social/location-city';
import classNames from 'classnames';
import Config from '../../config/base';
import jwtDecode from 'jwt-decode';
import InfoComponent from '../common/InfoComponent';
import ScreenListener from '../common/ScreenListener';

import { Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import {blue500, red500, greenA200} from 'material-ui/styles/colors';

import FontIcon from 'material-ui/FontIcon';

import Header from './VerticalHeader';


class LinkComponent extends React.Component {

  state = {
    activeKey: 1,
    open: false,
    screenWidth: window.innerWidth,
    screenHeight: window.innerHeight,
  }

  handleScreenChange = (w, h) => {
    this.setState({screenWidth: w, screenHeight: h});
  }

  handleSelect = (selectedKey) => {
    this.setState({activeKey: selectedKey});

    switch (selectedKey) {
      case 1:
        this.props.router.push('/');
        break;
      case 2:
        this.props.router.push('/push');
        break;
      case 3:
          this.props.router.push('/category');
          break;
      case 4:
        this.props.router.push('/product');
        break;
      case 5:
        this.props.router.push('/post');
        break;
      case 6:
        this.props.router.push('/about');
        break;
      case 7:
        this.props.router.push('/term');
        break;
      case 8:
        this.props.router.push('/setting');
        break;
      case 9:
        this.props.router.push('/account');
        break;
      // case 3:
      //   this.props.router.push('/category');
      //   break;
      // case 4:
      //   this.props.router.push('/subcategory');
      //   break;
      // case 5:
      //   this.props.router.push('/push');
      //   break;
      // case 6:
      //   this.props.router.push('/location');
      //   break;
      // case 7:
      //   this.props.router.push('/inventory');
      //   break;
      // case 8:
      //   this.props.router.push('/account');
      //   break;
      // case 9:
      //   this.props.router.push('/cymcategory');
      //   break;
      // case 10:
      //   this.props.router.push('/cym');
      //   break;
      // case 11:
      //   this.props.router.push('/drink');
      //   break;
      // case 12:
      //   this.props.router.push('/package');
      //   break;
      default:
        return;
    }
  }

  handleRouteReload = (pathname) => {
    if (pathname == '/push') {
      this.setState({activeKey: 2});
    }
    else if (pathname.includes('/category')) {
      this.setState({activeKey: 3});
    }
    else if (pathname.includes('/product')) {
      this.setState({activeKey: 4});
    }
    else if (pathname.includes('/post')) {
       this.setState({activeKey: 5});
    }
    else if (pathname.includes('/about')) {
       this.setState({activeKey: 6});
    }
    else if (pathname.includes('/term')) {
       this.setState({activeKey: 7});
    }
    else if (pathname.includes('/setting')) {
       this.setState({activeKey: 8});
    }
    else if (pathname.includes('/account')) {
      this.setState({activeKey: 9});
    }
    // else if (pathname.includes('/push')) {
    //    this.setState({activeKey: 3});
    // }
    // else if (pathname.includes('/category')) {
    //   this.setState({activeKey: 3});
    // }
    // else if (pathname.includes('/subcategory')) {
    //   this.setState({activeKey: 4});
    // }
    // else if (pathname.includes('/product')) {
    //   this.setState({activeKey: 5});
    // }
    // else if (pathname.includes('/location')) {
    //   this.setState({activeKey: 6});
    // }
    // else if (pathname.includes('/inventory')) {
    //   this.setState({activeKey: 7});
    // }
    // else if (pathname.includes('/cymcategory')) {
    //   this.setState({activeKey: 9});
    // }
    // else if (pathname.includes('/cym')) {
    //   this.setState({activeKey: 10});
    // }
    // else if (pathname.includes('/drink')) {
    //   this.setState({activeKey: 11});
    // }
    // else if (pathname.includes('/package')) {
    //   this.setState({activeKey: 12});
    // }
    else if (pathname == '/') {
      this.setState({activeKey: 1});
    }
  }

  componentWillMount(){
    let {pathname} = this.props.router.location;
    this.handleRouteReload(pathname);

    let user = jwtDecode(sessionStorage.getItem(Config.MERCHANT_NAME));
    this.setState({
      tabs: user.cmsTabs.split(',')
    });
  }

  handleClick = (event) => {
    const {actions} = this.props;
    actions.logoutUser(this.props.router);
  }

  handleInfo = (event) => {
    this.setState({open: true});
  }

  handleClose = () => {
    this.setState({open:false});
  }

  handleAccount = () => {
    const {router} = this.props;
    this.setState({activeKey:0});
    router.push('/credential');
  }

  linkClass = (menu) => {
    const {role, tabs} = this.state;
    if (tabs[0] == 'all') {
      return 'visible';
    }else
      return (tabs.includes(menu)) ? 'visible' : 'hide';
  }

  render() {
    const role = jwtDecode(sessionStorage.getItem(Config.MERCHANT_NAME)).role;
    let superUserOnly = classNames({
      'hide': !Config.SUPERUSER.includes(role),
      'visible': Config.SUPERUSER.includes(role)
    });
    let date = new Date();

    return (
      <div className="hidden-xs nav-fixed">
        <Header router={this.props.router}>
          <div className='nav-bar-container nav-scroll' style={{ height: (this.state.screenHeight - 150)}}>
            <ul className='nav-bar'>
              <Nav stacked activeKey={this.state.activeKey} onSelect={this.handleSelect}>
                <NavItem eventKey={1} className={this.linkClass('profile')}>
                  <Business className='nav-icon'/>Profile</NavItem>
                <NavItem eventKey={2} className={this.linkClass('push')}>
                  <Chat className='nav-icon'/>Push</NavItem>
                <NavItem eventKey={3} className={this.linkClass('category')}>
                  <Category className='nav-icon'/>Category</NavItem>
                <NavItem eventKey={4} className={this.linkClass('product')}>
                  <Cart className='nav-icon'/>Products</NavItem>
                <NavItem eventKey={5} className={this.linkClass('post')}>
                  <Offer className='nav-icon'/>Promos</NavItem>
                <NavItem eventKey={6} className={this.linkClass('about')}>
                  <City className='nav-icon'/>About Us</NavItem>
                <NavItem eventKey={7} className={this.linkClass('term')}>
                  <Assignment className='nav-icon'/>Terms and Conditions</NavItem>
                <NavItem eventKey={8} className={this.linkClass('setting')}>
                  <Setting className='nav-icon'/>Wallet Settings</NavItem>
                {/*
                <NavItem eventKey={9} className={this.linkClass('cymcategory')}>
                  <CYMCategory className='nav-icon'/>CYM Category</NavItem>
                <NavItem eventKey={10} className={this.linkClass('cym')}>
                  <CYM className='nav-icon'/>CYM</NavItem>
                <NavItem eventKey={3} className={this.linkClass('category')}>
                  <Category className='nav-icon'/>Category</NavItem>
                <NavItem eventKey={4} className={this.linkClass('subcategory')}>
                  <Subcategory className='nav-icon'/>Subcategory</NavItem>
                <NavItem eventKey={5} className={this.linkClass('product')}>
                  <Cart className='nav-icon'/>Products</NavItem>
                <NavItem eventKey={12} className={this.linkClass('package')}>
                  <Package className='nav-icon'/>Package</NavItem>
                <NavItem eventKey={11} className={this.linkClass('drink')}>
                  <Drink className='nav-icon'/>Drink</NavItem>
                <NavItem eventKey={6} className={this.linkClass('location')}>
                  <Branch className='nav-icon'/>Location</NavItem>
                <NavItem eventKey={7} className={this.linkClass('inventory')}>
                  <Inventory className='nav-icon'/>Inventory</NavItem>*/}
                <NavItem eventKey={9} className={this.linkClass('account')}>
                  <Account className='nav-icon'/>Accounts</NavItem>
              </Nav>
            </ul>
          </div>
        </Header>


        <ScreenListener onScreenChange={this.handleScreenChange}/>

        <div className="footer-container">
          <div className="footer-btn footer-left" onClick={this.handleAccount}>
            <Account className='footer-icon'/>
          </div>
          <div className="footer-btn" onClick={this.handleInfo}>
            <Info className='footer-icon'/>
          </div>
          <div className="footer-btn" onClick={this.handleClick}>
            <Lock className='footer-icon'/>
          </div>
        </div>

        {/* Info */}
        <InfoComponent
          open={this.state.open}
          onRequestClose={this.handleClose}/>
      </div>
    )
  }
}

LinkComponent.displayName = 'NavigationLinkComponent';

// Uncomment properties you need
// LinkComponent.propTypes = {};
// LinkComponent.defaultProps = {};
//  <li><Link to="" onClick={ () => handleClick(property) }>Logout</Link></li>

export default LinkComponent;
