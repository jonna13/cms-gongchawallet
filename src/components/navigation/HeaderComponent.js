'use strict';

// import React from 'react';
import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import AppBar from 'material-ui/AppBar';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/menu';
import { ListItem} from 'material-ui/List';

let logo = require('../../images/logo.png');

class Logged extends Component{
  state = {
    openMenu: false
  }

  handleNavigate = (selectedLocation) =>{
    const {pathname} = this.props.router.location;

    if (pathname == selectedLocation) return;
    else{
      this.setState({openMenu: false});
      this.props.router.push(selectedLocation);
    }
  }

  handleOnRequestChange = (value) => {
    this.setState({
      openMenu: value
    });
  }

  handleLogout = () => {
    const {actions} = this.props;
    actions.logoutUser(this.props.router);
  }

  render(){
    return(
      <IconMenu
        className='header-icon-menu'
        iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
        open={this.state.openMenu}
        onRequestChange={this.handleOnRequestChange}
      >
        <MenuItem primaryText="Profile" onClick={ ()=>{this.handleNavigate('/')} }/>
        <MenuItem primaryText="Push" onClick={ ()=>{this.handleNavigate('/push')} }/>
        <MenuItem primaryText="Category" onClick={ ()=>{this.handleNavigate('/category')} }/>
        <MenuItem primaryText="Products" onClick={ ()=>{this.handleNavigate('/product')} }/>
        <MenuItem primaryText="Promos" onClick={ ()=>{this.handleNavigate('/post')} }/>
        <MenuItem primaryText="Earn Settings" onClick={ ()=>{this.handleNavigate('/setting/EARNGC23143DbVSs')} }/>
        <MenuItem primaryText="Accounts" onClick={ ()=>{this.handleNavigate('/account')} }/>
        {/*<MenuItem primaryText="Category" onClick={ ()=>{this.handleNavigate('/category')} }/>
        <MenuItem primaryText="Subcategory" onClick={ ()=>{this.handleNavigate('/subcategory')} }/>
        <MenuItem primaryText="Products" onClick={ ()=>{this.handleNavigate('/product')} }/>
        <MenuItem primaryText="Flavors" onClick={ ()=>{this.handleNavigate('/flavor')} }/>
        <MenuItem primaryText="Addons" onClick={ ()=>{this.handleNavigate('/addon')} }/>
        <MenuItem primaryText="Drinks" onClick={ ()=>{this.handleNavigate('/drink')} }/>
        <MenuItem primaryText="Dips" onClick={ ()=>{this.handleNavigate('/dips')} }/>
        <MenuItem primaryText="Sides" onClick={ ()=>{this.handleNavigate('/side')} }/>
        <MenuItem primaryText="Upgrades" onClick={ ()=>{this.handleNavigate('/upgrade')} }/>*/}
        <MenuItem primaryText="About" onClick={this.props.onInfo} />
        <MenuItem primaryText="Logout" onClick={this.handleLogout} />
      </IconMenu>
    )
  }
}

class HeaderComponent extends Component {
  render() {
    return (
      <div>
        <AppBar
          className='header-container'
          iconElementLeft={<img className='img-header' src={logo}/>}
          iconElementRight={ <Logged {...this.props} /> }
        />

      </div>
    )
  }
}

HeaderComponent.displayName = 'NavigationHeaderComponent';

// Uncomment properties you need
HeaderComponent.propTypes = {
  actions: PropTypes.object.isRequired
};
// HeaderComponent.defaultProps = {};

function mapStateToProps(state) {
  const props = { isLogged_in : state.authReducer };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    authAction: require('../../actions/authAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.authAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderComponent);
