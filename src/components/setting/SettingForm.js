/**
 * Created by jonna on 1/5/18.
 */
import React from 'react';
import Config from '../../config/base';
import { TextAreaField } from '../common/TextComponent';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';
import {CleanWord} from '../common/Utility';

class SettingForm extends React.Component{

  state = {
    shouldDisplay: false,
  };

  componentWillMount(){
    if (!_.isEmpty(this.props.data.records)){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.records)) {
        this.setState({shouldDisplay: true});
      }
    }
  }


  render(){
    const {data} = this.props;
    console.info('SettingForm', data);
    console.info('shouldDisplay', this.state.shouldDisplay);

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <br/>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='pull-left content-field-holder'>
                <div className='profile-info'>Rewards Settings</div>
                <br/>
                <TextField
                  name="name"
                  hintText="Name"
                  floatingLabelText="Name"
                  defaultValue={(data.records.name) ? data.records.name : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                  disabled={true}
                /><br />
                <TextField
                  name="camount"
                  type="number"
                  hintText="Amount"
                  floatingLabelText="Amount"
                  defaultValue={(data.records.camount) ? data.records.camount : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <TextField
                  name="cpoints"
                  hintText="LYL"
                  floatingLabelText="LYL"
                  defaultValue={(data.records.cpoints) ? data.records.cpoints : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
              </div>
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='pull-left content-field-holder'>
                <div className='profile-info'>Conversion Settings</div>
                <br/>
                <TextField
                  name="php"
                  type="number"
                  hintText="PHP"
                  floatingLabelText="PHP"
                  defaultValue={(data.records.php) ? data.records.php : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <TextField
                  name="xem"
                  hintText="XEM"
                  floatingLabelText="XEM"
                  defaultValue={(data.records.xem) ? data.records.xem : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <TextField
                  name="btc"
                  hintText="BTC"
                  floatingLabelText="BTC"
                  defaultValue={(data.records.btc) ? data.records.btc : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <TextField
                  name="eth"
                  hintText="ETH"
                  floatingLabelText="ETH"
                  defaultValue={(data.records.eth) ? data.records.eth : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default SettingForm;
