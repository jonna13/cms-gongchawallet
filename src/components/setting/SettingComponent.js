/**
 * Created by jonna on 1/5/18.
 */
'use strict';
/* Core Components */
import React from 'react';
import { SettingButton } from '../common/buttons';
import Divider from 'material-ui/Divider';
import SettingDisplay from './SettingDisplay';
import jwtDecode from 'jwt-decode';
import Config from '../../config/base';

class SettingComponent extends React.Component {

  componentWillMount() {
    let userToken = jwtDecode(sessionStorage.getItem(Config.MERCHANT_NAME));
    if (userToken.cmsTabs.split(',')[0] == 'order') {
      this.props.router.push('/inventory');
      return;
    }

    let action = this.props.actions;
    action.getSetting();
  }

  onSettingChange = (e) => {
    const field = e.target.name;
    const setting = this.state.data;
    setting[field] = e.target.value;
    return this.setState({data: setting});
  }

  handleSettingEdit = () => {
    const {router} = this.props;
    router.push('/setting_edit');
    console.log("Handle Edit");
  }

  handleAccountEdit = () => {
    const {router} = this.props;
    router.push('/credential');
  }

  render() {
    return (
      <div>
        <h2 className='content-heading'>Setting Us</h2>
        <Divider className='content-divider'/>
        <SettingButton
          onSettingEdit={this.handleSettingEdit}
          onAccountEdit={this.handleAccountEdit} />
        <SettingDisplay
          setting={this.props.data}/>
      </div>
    );
  }
}

SettingComponent.displayName = 'SettingSettingComponent';

// Uncomment properties you need
// SettingComponent.propTypes = {};
// SettingComponent.defaultProps = {};

export default SettingComponent;
