/**
 * Created by jonna on 1/5/18.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';

import SettingForm from './SettingForm';

class SettingEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      setting: {
        name: '',
        description: '',
      }
    }
  }

  componentWillMount() {
    if (_.isEmpty(this.props.settingState.records)) {
      let action = this.props.actions;
      action.getSetting();
    }
    else {
      this.setState({
        setting: this.props.settingState.records
      });
    }
  }

  componentWillReceiveProps(nextProps){
    if (!_.isEmpty(nextProps.settingState.records)) {
      this.setState({
        setting: nextProps.settingState.records
      });
    }
  }

  // handle going back to setting
  handleReturn = () => {
    this.props.router.push('/setting');
  }

  // handle Update
  handleUpdate = () => {
    if (this.validateInput(this.state.setting)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_SETTING_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateSetting(this.state.setting, this.state.uploadImage, this.props.router);
          }
        });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    console.log('data', data);
    if (data.name == '') {
      dialogActions.openNotification('Oops! No title found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let setting = this.state.setting;
    setting[field] = e.target.value;
  }

  // handle image change
  handleImageChange = (e) => {
    this.setState({ uploadImage: e });
  }

  render(){
    console.info('SettingEditor', this.props.settingState);
    return(
      <div>
        <h2 className='content-heading'> Setting </h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        <UpdateButton handleOpen={this.handleUpdate}/>

        <SettingForm
          data={this.props.settingState}
          shouldEdit={true}
          onChange={this.handleData}
          onImageChange={this.handleImageChange}
          imageModule={this.state.imageModule}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    settingState: state.settingState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    settingAction: require('../../actions/settingAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.settingAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingEditor);
