/**
 * Created by jedachas on 3/8/17.
 */
import React from 'react';
import Config from '../../config/base';
import { TextAreaField } from '../common/TextComponent';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';

class LocationForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      shouldDisplay: false,
    };
  }

  componentWillMount(){
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({shouldDisplay: true});
      }
    }
  }

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  onLocFlagCheck = (e, val) => {
    this.props.onLocFlagChange(val);
  }

  render(){
    const {data} = this.props;

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='pull-left content-field-holder'>
                <TextField
                  name="name"
                  hintText="Enter Name"
                  floatingLabelText="Name"
                  defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                  onChange={this.props.onChange}
                /><br />
                <TextField
                  name="address"
                  hintText="Enter Address"
                  floatingLabelText="Address"
                  defaultValue={(data.selectedRecord.address) ? data.selectedRecord.address : '' }
                  onChange={this.props.onChange}
                  multiLine={true}
                  rows={4}
                /><br />
                <TextField
                  name="latitude"
                  hintText="Enter Latitude"
                  floatingLabelText="Latitude"
                  defaultValue={(data.selectedRecord.latitude) ? data.selectedRecord.latitude : '' }
                  onChange={this.props.onChange}
                /><br />
                <TextField
                  name="longitude"
                  hintText="Enter Longitude"
                  floatingLabelText="Longitude"
                  defaultValue={(data.selectedRecord.longitude) ? data.selectedRecord.longitude : '' }
                  onChange={this.props.onChange}
                /><br />
                <TextAreaField
                  name="businessHrs"
                  hintText="Enter Business Hours"
                  floatingLabelText="Business Hours"
                  defaultValue={(data.selectedRecord.businessHrs) ? data.selectedRecord.businessHrs : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  /><br />
              </div>
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='pull-left'>
                <TextField
                  name="branchCode"
                  hintText="Enter Branch Code"
                  floatingLabelText="Branch Code"
                  defaultValue={(data.selectedRecord.branchCode) ? data.selectedRecord.branchCode : '' }
                  onChange={this.props.onChange}
                /><br />
                <TextField
                  name="phone"
                  hintText="Enter Phone"
                  floatingLabelText="Phone"
                  defaultValue={(data.selectedRecord.phone) ? data.selectedRecord.phone : '' }
                  onChange={this.props.onChange}
                /><br />
                <TextField
                  name="email"
                  hintText="Enter Email"
                  floatingLabelText="Email"
                  defaultValue={(data.selectedRecord.email) ? data.selectedRecord.email : '' }
                  onChange={this.props.onChange}
                /><br />
                <Checkbox
                  label="locFlag"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.locFlag == 'active') ? true : false) : false }
                  onCheck={this.onLocFlagCheck}
                />
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default LocationForm;
