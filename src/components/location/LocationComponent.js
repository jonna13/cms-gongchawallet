'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import Config from '../../config/base';
import {AddButton} from '../common/buttons';
import {SearchBar,CheckStatus,PageSize} from '../common/widgets';

import LocationList from './LocationList';

class LocationComponent extends React.Component {

  handleAdd = () => {
    const {router} = this.props;
    router.push('/location_add');
  }

  handleEdit = (item) => {
    const {router} = this.props;
    router.push('/location/'+item.locID);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.locations;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.locations;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (text) => {
    let {pageinfo} = this.props.data.locations;
    pageinfo.searchFilter = text;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.locations;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.locations;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus
    }
    console.info('LocationComponent: param', params);
    actions.getLocations(params);
  }

  render() {
    const {pageinfo} = this.props.data.locations;
    const {data} = this.props;
    return (
      <div>
        <h2 className='content-heading'>Location</h2>
        <h5 className='content-record-label'>Records: <b>{ pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <AddButton
            handleOpen={this.handleAdd}/>
        <CheckStatus
            pageinfo={data.locations.pageinfo}
            handleChange={this.handleStatusChange}/>
        <PageSize
            pageinfo={data.locations.pageinfo}
            handleChange={this.handlePageSize}/>
        <SearchBar
            pageinfo={data.locations.pageinfo}
            handleChange={this.handleSearchChange}
            handleClose={this.handleSearchClose}/>

        { /* List */ }
        <LocationList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit} />
      </div>
    );
  }
}

LocationComponent.displayName = 'LocationLocationComponent';

// Uncomment properties you need
// LocationComponent.propTypes = {};
// LocationComponent.defaultProps = {};

export default LocationComponent;
