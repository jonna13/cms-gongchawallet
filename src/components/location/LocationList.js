/**
 * Created by jedachas on 3/9/17.
 */
 import React, {Component} from 'react';
 import ListTable from '../common/table/ListTableComponent';
 import GridRow from './LocationGridRow';

class LocationList extends Component {
  handleGetData = (params) => {
    this.props.actions.getLocations(params);
  }

  render(){
    let {locations} = this.props.data;
    const headers = [
      { title: '', value: ''},
      { title: 'Name', value: 'name'},
      { title: 'Branch Code', value: 'branchCode'},
      { title: 'Loyalty', value: 'locFlag'},
      { title: 'Status', value: 'status'}
    ];

    var rows = [];
    if (locations.records.length > 0) {
      locations.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}/>);
      })
    }

    return(
      <div className='content-container'>

        <ListTable
          headers={headers}
          data={this.props.data.locations}
          onGetData={this.handleGetData}>
            {rows}
        </ListTable>
      </div>
    );
  }

}

export default LocationList;
