/**
 * Created by jedachas on 3/9/17.
 */
 import React, {Component} from 'react';
 import ListTable from '../common/table/ListTableComponent';
 import Config from '../../config/base';

 import GridRow from './ProductGridRow';

class ProductList extends Component {
  handleGetData = (params) => {
    this.props.actions.getProducts(params);
  }

  render(){
    let {products} = this.props.data;
    const headers = [
      { title: '', value: ''},
      { title: 'Name', value: 'name'},
      { title: 'Category', value: 'categoryID'},
      { title: 'Unit', value: 'unit'},
      { title: 'Price', value: 'price'},
      { title: 'Status', value: 'status'}
    ];

    var rows = [];
    if (products.records.length > 0) {
      products.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}
          module={Config.IMAGE.PRODUCT}/>);
      });
    }

    return(
      <div className='content-container'>

        <ListTable
          headers={headers}
          data={this.props.data.products}
          onGetData={this.handleGetData}>
            {rows}
        </ListTable>
      </div>
    );
  }

}

export default ProductList;
