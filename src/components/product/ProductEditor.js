/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, AddButton, ReturnButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';

import ProductForm from './ProductForm';

class ProductEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      product: {
        name: '',
        productCode: '',
        description: '',
        categoryID: 'CAT8P141284aepJv',
        price: 0.00,
        unit: 0,
        order: 0,
        status: ''
      },
      uploadImage: {},
      imageModule: Config.IMAGE.PRODUCT,
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewProduct(this.props.params.id);
    }
    actions.getProductCategory();
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.productState.selectedRecord)) {
        this.setState({
          product: nextProps.productState.selectedRecord
        });
      }
    }
  }

  // handle going back to Product
  handleReturn = () => {
    this.props.router.push('/product');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    if (this.validateInput(this.state.product)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_PRODUCT_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateProduct(this.state.product, this.state.uploadImage, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    console.info('handleAdd:state', this.state);
    if (this.validateInput(this.state.product)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_PRODUCT_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addProduct(this.state.product, this.state.uploadImage, this.props.router);
          }
      });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.price == '') {
      dialogActions.openNotification('Oops! No price found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.unit == '') {
      dialogActions.openNotification('Oops! No unit found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let product = this.state.product;
    product[field] = e.target.value;
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.product['status'] = e;
  }

  handleCategoryChange = (value) => {
    this.state.product['categoryID'] = value;
  }

  // handle image change
  handleImageChange = (e) => {
    this.setState({ uploadImage: e });
  }

  render(){
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Product' : 'Add Product'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <AddButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <ProductForm
          data={this.props.productState}
          shouldEdit={ (this.props.params.id) ? true : false }
          onChange={this.handleData}
          onCategoryChange={this.handleCategoryChange}
          onImageChange={this.handleImageChange}
          imageModule={this.state.imageModule}
          onStatusChange={this.handleStatusChange}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    productState: state.productState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    productAction: require('../../actions/productAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.productAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductEditor);
