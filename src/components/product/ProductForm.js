/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import Config from '../../config/base';
import { TextAreaField } from '../common/TextComponent';
import { DefaultTextArea, LabelListField } from '../common/fields';
import { ListDrop} from '../common/lists';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import DatePicker from 'material-ui/DatePicker';
import MenuItem from 'material-ui/MenuItem';
import ImageUpload from '../common/ImageUpload';
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';


class ProductForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      shouldDisplay: false,
      category: ''
    };
  }

  componentWillMount(){
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({
          shouldDisplay: true,
          category: nextProps.data.selectedRecord.categoryID,
        });
      }
    }
  }

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  handleProductChange = (category) => {
    console.info('handleProductChange', category);
    if (category) {
      this.setState({category});
    }
    else{
      this.setState({category: '', subcategory: ''});
    }
    this.props.onCategoryChange(category);
  }

  render(){
    const {data} = this.props;
    console.log('data', data.selectedRecord);
    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <Col xs={6} sm={6} md={6} lg={6}>
              <div className='content-container'>
                <div className='item-image-box'>
                  <ImageUpload
                    onImageChange={this.props.onImageChange}
                    image={(data.selectedRecord.image) ? data.selectedRecord.image : '' }
                    imageModule={this.props.imageModule}/>
                </div>
              </div>
            </Col>
            <Col xs={6} sm={6} md={6} lg={6}>
              <div className='pull-left content-field-holder'>
                <TextField
                  disabled={(this.props.shouldEdit) ? true: false}
                  name="productCode"
                  hintText="Enter Product Code"
                  floatingLabelText="Product Code"
                  defaultValue={(data.selectedRecord.productCode) ? data.selectedRecord.productCode : '' }
                  onChange={this.props.onChange}
                /><br />
                <TextField
                  name="name"
                  hintText="Enter Name"
                  floatingLabelText="Name"
                  defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                  onChange={this.props.onChange}
                /><br />

                <DefaultTextArea
                  name="description"
                  hintText="Enter Description"
                  floatingLabelText="Description"
                  defaultValue={(data.selectedRecord.description) ? data.selectedRecord.description : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  regular={true}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  /><br />

                <TextField
                  name="price"
                  type="number"
                  hintText="Enter Price"
                  floatingLabelText="Price"
                  defaultValue={(data.selectedRecord.price) ? data.selectedRecord.price : '' }
                  onChange={this.props.onChange}
                /><br />
                <TextField
                  name="unit"
                  type="number"
                  hintText="Enter Unit"
                  floatingLabelText="Unit"
                  defaultValue={(data.selectedRecord.unit) ? data.selectedRecord.unit : '' }
                  onChange={this.props.onChange}
                /><br />
                <TextField
                  name="order"
                  type="number"
                  hintText="Enter Order"
                  floatingLabelText="Order"
                  defaultValue={(data.selectedRecord.order) ? data.selectedRecord.order : '' }
                  onChange={this.props.onChange}
                /><br />
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default ProductForm;
