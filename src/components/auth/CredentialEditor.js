'use strict';

import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import TextField from 'material-ui/TextField';
import Config from '../../config/base';
import RaisedButton from 'material-ui/RaisedButton';
import Divider from 'material-ui/Divider';
import {UpdateButton} from '../common/buttons';

import AuthApi from '../../api/auth.api';
const logo = require('../../images/logo.png');

class CredentialEditor extends React.Component {

    state = {
      credentials: {
        old_password: '',
        new_password: '',
        reenter: ''
      }
    }

    handleInput = (event) => {
      const field = event.target.name;
      const details = this.state.credentials;
      details[field] = event.target.value;
    }

    // handle going back to profile
    handleReturn = () => {
      this.props.router.push('/');
    }

    // handle Update
    handleUpdate = () => {
      if (this.validateInput(this.state.credentials)) {
        const {dialogActions} = this.props;
        dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
          Config.DIALOG_MESSAGE.UPDATE_CREDENTIAL_MESSAGE,
          Config.DIALOG_MESSAGE.CONFIRM_LABEL,
          Config.DIALOG_MESSAGE.CLOSE_LABEL,
          (result) => {
            if (result) {
              let {authActions} = this.props;
              authActions.updatePassword(this.state.credentials, this.props.router);
            }
          });
      }
    }

    validateInput = (data) => {
      let Expression = (/^(?=.*\d)(?=.*[a-z])[0-9a-zA-Z]{5,}$/);
      const {dialogActions} = this.props;
      if (!data.new_password.match(Expression)) {
        dialogActions.openNotification('Should have at least 1 number & 1 letter. 5 more characters', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
      if (data.old_password == '') {
        dialogActions.openNotification('Oops! please enter old password', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
      if (data.new_password == '') {
        dialogActions.openNotification('Oops! please enter new password', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
      if (data.reenter == '') {
        dialogActions.openNotification('Oops! please re-enter new password', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
      if (data.new_password != data.reenter) {
        dialogActions.openNotification('Oops! re-enter new password correctly', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }

      return true;
    }

    render() {
      return (
        <div>
          <h2 className='content-heading'> Credentials </h2>
          <Divider className='content-divider' />

          { /* Action Buttons */ }
          <UpdateButton handleOpen={this.handleUpdate}/>

          <TextField
            name="old_password"
            floatingLabelText="Old Password"
            type="password"
            onChange={this.handleInput}
          /><br />
          <TextField
            name="new_password"
            floatingLabelText="New Password"
            type="password"
            onChange={this.handleInput}
          /><br />
          <TextField
            name="reenter"
            floatingLabelText="Re-enter Password"
            type="password"
            onChange={this.handleInput}
          /><br />

        </div>
      );
    }

}

CredentialEditor.displayName = 'AuthCredentialEditor';



function mapStateToProps(state) {
  const props = {
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    dialogAction: require('../../actions/dialogAction.js'),
    authAction: require('../../actions/authAction.js'),
  };
  const actionMap = {
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
    authActions: bindActionCreators(actions.authAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(CredentialEditor);
