'use strict';

import React from 'react';
import TextField from 'material-ui/TextField';
import Config from '../../config/base';
import RaisedButton from 'material-ui/RaisedButton';

import AuthApi from '../../api/auth.api';
const logo = require('../../images/logo.png');

class LoginComponent extends React.Component {

    constructor(props){
      super(props);

      this.state = {
        credentials: {
          username: '',
          password: ''
        }
      }
    }

    handleInput = (event) => {
      const field = event.target.name;
      const details = this.state.credentials;
      details[field] = event.target.value;
      // return this.setState({ credentials: details });
    }

    onLogin = (event) => {
      console.info('onLogin', this.state);
      const { actions } = this.props;
      actions.loginUser(this.state.credentials, this.props.router);
    }

    handleKeyPress = (event) => {
      if(event.key == 'Enter') {
         this.onLogin();
      }
    }

    render() {
      return (
        <div className="login-component">
          <img src={logo} className='login-img'/>
          <TextField
            name="username"
            floatingLabelText="Username"
            defaultValue={this.state.credentials.username}
            onChange={this.handleInput}
            onKeyPress={this.handleKeyPress}
          /><br />
          <TextField
            name="password"
            floatingLabelText="Password"
            type="password"
            defaultValue={this.state.credentials.password}
            onChange={this.handleInput}
            onKeyPress={this.handleKeyPress}
          /><br />

          <RaisedButton
            label="Login"
            primary={true}
            onClick={this.onLogin}
            className="login-button"  />

        </div>
      );
    }

}

LoginComponent.displayName = 'AuthLoginComponent';

// Uncomment properties you need
// LoginComponent.propTypes = {};
// LoginComponent.defaultProps = {};

export default LoginComponent;
