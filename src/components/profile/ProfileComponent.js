/**
 * Created by jedachas on 2/9/17.
 */
'use strict';
/* Core Components */
import React from 'react';
import { ProfileButton } from '../common/buttons';
import Divider from 'material-ui/Divider';
import ProfileDisplay from './ProfileDisplay';
import jwtDecode from 'jwt-decode';
import Config from '../../config/base';

class ProfileComponent extends React.Component {

  componentWillMount() {
    let userToken = jwtDecode(sessionStorage.getItem(Config.MERCHANT_NAME));
    if (userToken.cmsTabs.split(',')[0] == 'order') {
      this.props.router.push('/inventory');
      return;
    }

    let action = this.props.actions;
    action.getProfile();
  }

  onProfileChange = (e) => {
    const field = e.target.name;
    const profile = this.state.data;
    profile[field] = e.target.value;
    return this.setState({data: profile});
  }

  handleProfileEdit = () => {
    const {router} = this.props;
    router.push('/profile_edit');
  }

  handleAccountEdit = () => {
    const {router} = this.props;
    router.push('/credential');
  }

  render() {
    return (
      <div>
        <h2 className='content-heading'>Profile and Settings</h2>
        <Divider className='content-divider'/>
        <ProfileButton
          onProfileEdit={this.handleProfileEdit}
          onAccountEdit={this.handleAccountEdit} />
        <ProfileDisplay
          profile={this.props.data}/>
      </div>
    );
  }
}

ProfileComponent.displayName = 'ProfileProfileComponent';

// Uncomment properties you need
// ProfileComponent.propTypes = {};
// ProfileComponent.defaultProps = {};

export default ProfileComponent;
