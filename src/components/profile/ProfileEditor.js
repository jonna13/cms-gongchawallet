/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';

import ProfileForm from './ProfileForm';

class ProfileEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      profile: {
        company: '',
        profilePic: '',
      },
      uploadImage: {},
      imageModule: Config.IMAGE.PROFILE,
    }
  }

  componentWillMount() {
    if (_.isEmpty(this.props.profileState.records)) {
      let action = this.props.actions;
      action.getProfile();
    }
    else {
      this.setState({
        profile: this.props.profileState.records
      });
    }
  }

  componentWillReceiveProps(nextProps){
    if (!_.isEmpty(nextProps.profileState.records)) {
      this.setState({
        profile: nextProps.profileState.records
      });
    }
  }

  // handle going back to profile
  handleReturn = () => {
    this.props.router.push('/');
  }

  // handle Update
  handleUpdate = () => {
    if (this.validateInput(this.state.profile)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_PROFILE_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateProfile(this.state.profile, this.state.uploadImage, this.props.router);
          }
        });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    console.log('data', data);
    if (data.company == '') {
      dialogActions.openNotification('Oops! No company found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (this.state.profile.profilePic == '') {
      if (_.isEmpty(this.state.uploadImage)) {
        dialogActions.openNotification('Oops! No image found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
    }

    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let profile = this.state.profile;
    profile[field] = e.target.value;
  }

  // handle image change
  handleImageChange = (e) => {
    this.setState({ uploadImage: e });
  }

  render(){
    console.info('ProfileEditor', this.props.profileState);
    return(
      <div>
        <h2 className='content-heading'> Profile </h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        <UpdateButton handleOpen={this.handleUpdate}/>

        <ProfileForm
          data={this.props.profileState}
          shouldEdit={true}
          onChange={this.handleData}
          onImageChange={this.handleImageChange}
          imageModule={this.state.imageModule}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    profileState: state.profileState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    profileAction: require('../../actions/profileAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.profileAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileEditor);
