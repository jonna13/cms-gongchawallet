/**
 * Created by jedachas on 2/9/17.
 */
import React from 'react';
import Config from '../../config/base';
import { Grid, Row, Col } from 'react-bootstrap';

import HttpIcon from 'material-ui/svg-icons/file/cloud';
import EmailIcon from 'material-ui/svg-icons/communication/email';
import PhoneIcon from 'material-ui/svg-icons/hardware/smartphone';
import ContactIcon from 'material-ui/svg-icons/communication/phone';
import Address from 'material-ui/svg-icons/communication/location-on';
import Person from 'material-ui/svg-icons/action/face';


require('styles/profile.css');

class ProfileDisplay extends React.Component {
  createMarkup = () => {
    return {__html: 'First &middot; Second'};
  }

  render(){
    if (this.props.profile.status == 'Success') {
      const {records} = this.props.profile;
      let path = Config.PROJECT_PATH + '' + Config.IMAGE.PATH + '' +
        Config.IMAGE.PROFILE + '/full/' + records.profilePic;

      let contactPerson = ((records.lname) ? records.lname + ', ' : '') +
        ((records.fname) ? records.fname : '') +
        ((records.mname) ? ' ' + records.mname : '');
      return(
        <div className='content-container'>
          <Grid fluid>
            <Row>
              <Col xs={0} sm={4} md={4} lg={4} >
                <div className='profile-image'>
                  <img src={path} />
                </div>
              </Col>
              <Col xs={12} sm={8} md={8} lg={8} >
                <div>
                  <h1 className='profile-label-company'>{records.company} </h1>
                  <div className='profile-info' dangerouslySetInnerHTML={{'__html':records.about}} ></div>
                </div>
                <div className='profile-icon-parent'>

                  { /* Icon Communications */ }
                  <Grid fluid className='col-no-padding'>
                    <Row>
                      <Col xs={12} sm={12} md={6} lg={6} className='col-no-padding'>
                        <div className='profile-icon-section'>
                          <HttpIcon color={Config.MUITHEME.palette.primary1Color} className='profile-icon'/>
                          <span className={ (records.website) ? 'profile-caption' : 'profile-caption-empty' }>
                            {records.website || 'n/a' }</span>
                        </div>
                        <div className='profile-icon-section'>
                          <EmailIcon color={Config.MUITHEME.palette.primary1Color} className='profile-icon'/>
                          <span className={ (records.email) ? 'profile-caption' : 'profile-caption-empty' }>
                            {records.email || 'n/a'}</span>
                        </div>
                        <div className='profile-icon-section'>
                          <Person color={Config.MUITHEME.palette.primary1Color} className='profile-icon'/>
                          <span className={ (contactPerson) ? 'profile-caption' : 'profile-caption-empty' }>
                            {contactPerson || 'n/a'}</span>
                        </div>
                      </Col>
                      <Col xs={12} sm={12} md={6} lg={6} className='col-no-padding'>
                        <div className='profile-icon-section'>
                          <PhoneIcon color={Config.MUITHEME.palette.primary1Color} className='profile-icon'/>
                          <span className={ (records.mobile) ? 'profile-caption' : 'profile-caption-empty' }>
                          { records.mobile || 'n/a' }</span>
                        </div>
                        <div className='profile-icon-section'>
                          <ContactIcon color={Config.MUITHEME.palette.primary1Color} className='profile-icon'/>
                          <span className={ (records.landline) ? 'profile-caption' : 'profile-caption-empty' }>
                            { records.landline || 'n/a' }</span>
                        </div>
                      </Col>
                      <Col xs={12} sm={12} md={12} lg={12} className='col-no-padding'>
                        <div className='profile-icon-section'>
                          <Address color={Config.MUITHEME.palette.primary1Color} className='profile-icon'/>
                            { (records.address) ?
                              <span className='profile-caption'
                                dangerouslySetInnerHTML={{'__html':records.address}} >
                              </span> :
                              <span className='profile-caption-empty'>'n/a'</span>
                             }
                        </div>
                      </Col>
                    </Row>
                  </Grid>



                </div>
              </Col>
            </Row>
          </Grid>
        </div>
      );
    }
    else{
      return null;
    }
  }
}

export default ProfileDisplay;
