/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import Config from '../../config/base';
import { TextAreaField } from '../common/TextComponent';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';
import {CleanWord} from '../common/Utility';

class ProductForm extends React.Component{

  state = {
    shouldDisplay: false,
  };

  componentWillMount(){
    if (!_.isEmpty(this.props.data.records)){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.records)) {
        this.setState({shouldDisplay: true});
      }
    }
  }


  render(){
    const {data} = this.props;
    console.info('ProfileForm', data);
    console.info('shouldDisplay', this.state.shouldDisplay);

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-container'>
                <div className='item-image-box'>
                  <ImageUpload image={null}
                    onImageChange={this.props.onImageChange}
                    image={(data.records.profilePic) ? data.records.profilePic : '' }
                    imageModule={this.props.imageModule}/>
                </div>
              </div>
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='pull-left content-field-holder'>
                <TextField
                  name="company"
                  hintText="Enter Company"
                  floatingLabelText="Company"
                  defaultValue={(data.records.company) ? data.records.company : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextAreaField
                  name="about"
                  hintText="Enter About"
                  floatingLabelText="About"
                  defaultValue={(data.records.about) ? CleanWord(data.records.about) : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  regular={true}
                  /><br />
                <TextField
                  name="email"
                  hintText="Enter Email"
                  floatingLabelText="Email"
                  defaultValue={(data.records.email) ? data.records.email : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="website"
                  hintText="Enter Website"
                  floatingLabelText="Website"
                  defaultValue={(data.records.website) ? data.records.website : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextAreaField
                  name="address"
                  hintText="Enter Address"
                  floatingLabelText="Address"
                  defaultValue={(data.records.address) ? CleanWord(data.records.address) : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  regular={true}
                  /><br />
                <TextField
                  name="fname1"
                  hintText="Enter First Name"
                  floatingLabelText="First Name"
                  defaultValue={(data.records.fname) ? data.records.fname : '' }
                  onChange={this.props.onChange}
                /><br />
                <TextField
                  name="lname"
                  hintText="Enter Last Name"
                  floatingLabelText="Last Name"
                  defaultValue={(data.records.lname) ? data.records.lname : '' }
                  onChange={this.props.onChange}
                /><br />
                <TextField
                  name="mname"
                  hintText="Enter Middle Name"
                  floatingLabelText="Middle Name"
                  defaultValue={(data.records.mname) ? data.records.mname : '' }
                  onChange={this.props.onChange}
                /><br />
                <TextField
                  name="landline"
                  hintText="Enter Landline"
                  floatingLabelText="Landline"
                  defaultValue={(data.records.landline) ? data.records.landline : '' }
                  onChange={this.props.onChange}
                /><br />
                <TextField
                  name="mobile"
                  hintText="Enter Mobile"
                  floatingLabelText="Mobile"
                  defaultValue={(data.records.mobile) ? data.records.mobile : '' }
                  onChange={this.props.onChange}
                /><br />
                <TextField
                  name="fax"
                  hintText="Enter Fax"
                  floatingLabelText="Fax"
                  defaultValue={(data.records.fax) ? data.records.fax : '' }
                  onChange={this.props.onChange}
                /><br />

              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default ProductForm;
