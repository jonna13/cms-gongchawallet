/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton, AddButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';
import update from 'react-addons-update';
import {EmailIsValid} from '../common/Utility';

import CymForm from './CymForm';


class CymEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      cym: {
        cymCategoryID: '',
        name: '',
        order:'',
        status: false
      }
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewCym(this.props.params.id);
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.cymState.selectedRecord)) {
        this.setState({
          cym: nextProps.cymState.selectedRecord
        });
      }
    }
  }

  // handle going back
  handleReturn = () => {
    const {actions} = this.props;
    actions.removeSelectedRecord();
    this.props.router.push('/cym');
  }

  // handle Update
  handleUpdate = () => {
    const {dialogActions} = this.props;
    if (this.validateInput(this.state.cym)) {
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_CYM_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateCym(this.state.cym, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.cym)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_CYM_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            console.log('thisstateCYM', this.state.cym);
            actions.addCym(this.state.cym, this.props.router);
          }
      });
    }
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let cym = this.state.cym;
    cym[field] = e.target.value;
    console.log('filed', cym[field]);
  }

  handleChangeCat = (value) => {
    this.state.cym['cymCategoryID'] = value;
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.cymCategoryID == '') {
      dialogActions.openNotification('Oops! No category found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.order == '') {
      dialogActions.openNotification('Oops! No order found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

    return true;
  }

  // handle status locFlag
  handleLocFlagChange = (e) => {
    this.state.cym['locFlag'] = e;
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.cym['status'] = e;
  }


  render(){
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Cym' : 'Add Cym'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <AddButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <CymForm
          data={this.props.cymState}
          shouldEdit={ (this.props.params.id) ? true : false }
          onChange={this.handleData}
          onChangeCat={this.handleChangeCat}
          onStatusChange={this.handleStatusChange}
          onLocFlagChange={this.handleLocFlagChange}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    cymState: state.cymState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    cymAction: require('../../actions/cymAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.cymAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(CymEditor);
