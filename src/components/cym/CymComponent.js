'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import Config from '../../config/base';
import {AddButton} from '../common/buttons';
import {SearchBar,CheckStatus,PageSize} from '../common/widgets';

import CymList from './CymList';

class CymComponent extends React.Component {

  handleAdd = () => {
    const {router} = this.props;
    router.push('/cym_add');
  }

  handleEdit = (item) => {
    const {router} = this.props;
    router.push('/cym/'+item.cymID);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.cyms;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.cyms;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (text) => {
    let {pageinfo} = this.props.data.cyms;
    pageinfo.searchFilter = text;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.cyms;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.cyms;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus
    }
    actions.getCyms(params);
  }

  render() {
    const {pageinfo} = this.props.data.cyms;
    const {data} = this.props;
    console.log('dataCYM', data);
    return (
      <div>
        <h2 className='content-heading'>Cym</h2>
        <h5 className='content-record-label'>Records: <b>{ pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <AddButton
            handleOpen={this.handleAdd}/>
        <CheckStatus
            pageinfo={data.cyms.pageinfo}
            handleChange={this.handleStatusChange}/>
        <PageSize
            pageinfo={data.cyms.pageinfo}
            handleChange={this.handlePageSize}/>
        <SearchBar
            pageinfo={data.cyms.pageinfo}
            handleChange={this.handleSearchChange}
            handleClose={this.handleSearchClose}/>

        { /* List */ }
        <CymList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit} />
      </div>
    );
  }
}

CymComponent.displayName = 'CymCymComponent';

// Uncomment properties you need
// CymComponent.propTypes = {};
// CymComponent.defaultProps = {};

export default CymComponent;
