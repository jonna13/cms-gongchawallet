/**
 * Created by jedachas on 3/9/17.
 */
 import React, {Component} from 'react';
 import ListTable from '../common/table/ListTableComponent';
 import GridRow from './CymGridRow';

class CymList extends Component {

  handleGetData = (params) => {
    this.props.actions.getCyms(params);
  }

  render(){
    let {cyms} = this.props.data;
    const headers = [
      { title: '', value: ''},
      { title: 'Name', value: 'name'},
      { title: 'Status', value: 'status'}
    ];

    var rows = [];
    if (cyms.records.length > 0) {
      cyms.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}/>);
      })
    }

    return(
      <div className='content-container'>

        <ListTable
          headers={headers}
          data={this.props.data.cyms}
          onGetData={this.handleGetData}>
            {rows}
        </ListTable>
      </div>
    );
  }

}

export default CymList;
