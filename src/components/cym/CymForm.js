/**
 * Created by jedachas on 3/8/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Config from '../../config/base';
import { TextAreaField } from '../common/TextComponent';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

class CymForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      cymCategoryID: '',
      shouldDisplay: false
    };
  }

  componentWillMount(){
    this.props.actions.getCategory();
    console.log('getCat', this.props.actions);
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});

    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({shouldDisplay: true});
      }
    }
  }

  handleCategoryChange = (e, idx, cymCategoryID) => {
    this.setState({cymCategoryID});
    this.props.onChangeCat(cymCategoryID);
   }

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  onLocFlagCheck = (e, val) => {
    this.props.onLocFlagChange(val);
  }

  render(){
    const {data} = this.props;
    console.log('selectedRecord', data.selectedRecord);
    var category = [];
        if (data.cyms.category.length > 0) {
          data.cyms.category.forEach((val, key) => {
            category.push(
              <MenuItem
              key={key}
              value={val.cymCategoryID}
              primaryText={val.name}
              />
            );
          });
        }

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='pull-left content-field-holder'>
                <SelectField
                  floatingLabelText="Category"
                  value={(data.selectedRecord.cymCategoryID) ? data.selectedRecord.cymCategoryID : this.state.cymCategoryID}
                  onChange={this.handleCategoryChange}
                  autoWidth={true}
                >
                  {category}
                </SelectField><br />
                <TextField
                  name="name"
                  hintText="Enter Name"
                  floatingLabelText="Name"
                  defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                  onChange={this.props.onChange}
                /><br />
                <TextField
                  name="order"
                  type="number"
                  hintText="Enter Order"
                  floatingLabelText="Order"
                  defaultValue={(data.selectedRecord.order) ? data.selectedRecord.order : '' }
                  onChange={this.props.onChange}
                /><br />
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

CymForm.defaultProps = {
};

function mapStateToProps(state) {
  const props = { cymState: state.cymState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    cymAction: require('../../actions/cymAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.cymAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(CymForm);
