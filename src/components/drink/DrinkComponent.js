'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import Config from '../../config/base';
import {AddButton} from '../common/buttons';
import {SearchBar,CheckStatus,PageSize} from '../common/widgets';

import DrinkList from './DrinkList';

class DrinkComponent extends React.Component {

  handleAdd = () => {
    const {router} = this.props;
    router.push('/drink_add');
  }

  handleEdit = (item) => {
    const {router} = this.props;
    router.push('/drink/'+item.drinkID);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.drinks;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.drinks;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (text) => {
    let {pageinfo} = this.props.data.drinks;
    pageinfo.searchFilter = text;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.drinks;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.drinks;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus
    }
    actions.getDrinks(params);
  }

  render() {
    const {pageinfo} = this.props.data.drinks;
    const {data} = this.props;
    return (
      <div>
        <h2 className='content-heading'>Drink Group</h2>
        <h5 className='content-record-label'>Records: <b>{ pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <AddButton
            handleOpen={this.handleAdd}/>
        <CheckStatus
            pageinfo={data.drinks.pageinfo}
            handleChange={this.handleStatusChange}/>
        <PageSize
            pageinfo={data.drinks.pageinfo}
            handleChange={this.handlePageSize}/>
        <SearchBar
            pageinfo={data.drinks.pageinfo}
            handleChange={this.handleSearchChange}
            handleClose={this.handleSearchClose}/>

        { /* List */ }
        <DrinkList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit} />
      </div>
    );
  }
}

export default DrinkComponent;
