/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton, AddButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';

import DrinkForm from './DrinkForm';

class DrinkEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      drink: {
        prodID: '',
        name: '',
        unit: '',
        order: '',
        status: ''
      },
      uploadImage: {},
      imageModule: Config.IMAGE.DRINK,
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewDrink(this.props.params.id);
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.drinkState.selectedRecord)) {
        this.setState({
          drink: nextProps.drinkState.selectedRecord
        });
      }
    }
  }

  // handle going back to Drink
  handleReturn = () => {
    this.props.router.push('/drink');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    if (this.validateInput(this.state.drink)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_DRINK_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateDrink(this.state.drink, this.state.uploadImage, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.drink)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_DRINK_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addDrink(this.state.drink, this.state.uploadImage, this.props.router);
          }
      });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let drink = this.state.drink;
    drink[field] = e.target.value;
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.drink['status'] = e;
  }

  // handle level
  handleProductChange = (e) => {
    this.state.drink['prodID'] = e;
    this.state.drink['name'] = this.props.drinkState.products.filter( val => {return val.prodID == e})[0].name;
  }

  handleUnitChange = e => {
    this.state.drink['unit'] = e;
  }

  // handle image change
  handleImageChange = (e) => {
    this.setState({ uploadImage: e });
  }

  render(){
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Drink Group' : 'Add Drink Group'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <AddButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <DrinkForm
          data={this.props.drinkState}
          shouldEdit={ (this.props.params.id) ? true : false }
          onChange={this.handleData}
          onProductChange={this.handleProductChange}
          onUnitChange={this.handleUnitChange}
          onStatusChange={this.handleStatusChange}
          onImageChange={this.handleImageChange}
          imageModule={this.state.imageModule}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    drinkState: state.drinkState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    drinkAction: require('../../actions/drinkAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.drinkAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(DrinkEditor);
