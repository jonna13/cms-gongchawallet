/**
 * Created by jedachas on 3/9/17.
 */
 import React, {Component} from 'react';
 import ListTable from '../common/table/ListTableComponent';
 import Config from '../../config/base';

 import GridRow from './DrinkGridRow';

class DrinkList extends Component {
  handleGetData = (params) => {
    this.props.actions.getDrinks(params);
  }

  render(){
    let {drinks} = this.props.data;
    const headers = [
      { title: '', value: ''},
      { title: 'Name', value: 'name'},
      { title: 'Order', value: 'order'},
      { title: 'Status', value: 'status'}
    ];

    var rows = [];
    if (drinks.records.length > 0) {
      drinks.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}
          module={Config.IMAGE.DRINK}/>);
      });
    }

    return(
      <div className='content-container'>

        <ListTable
          headers={headers}
          data={this.props.data.drinks}
          onGetData={this.handleGetData}>
            {rows}
        </ListTable>
      </div>
    );
  }

}

export default DrinkList;
