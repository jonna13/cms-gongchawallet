/**
 * Created by jonna on 1/5/18.
 */
'use strict';
/* Core Components */
import React from 'react';
import { TermButton } from '../common/buttons';
import Divider from 'material-ui/Divider';
import TermDisplay from './TermDisplay';
import jwtDecode from 'jwt-decode';
import Config from '../../config/base';

class TermComponent extends React.Component {

  componentWillMount() {
    let userToken = jwtDecode(sessionStorage.getItem(Config.MERCHANT_NAME));
    if (userToken.cmsTabs.split(',')[0] == 'order') {
      this.props.router.push('/inventory');
      return;
    }

    let action = this.props.actions;
    action.getTerm();
  }

  onTermChange = (e) => {
    const field = e.target.name;
    const term = this.state.data;
    term[field] = e.target.value;
    return this.setState({data: term});
  }

  handleTermEdit = () => {
    const {router} = this.props;
    router.push('/term_edit');
    console.log("Handle Edit");
  }

  handleAccountEdit = () => {
    const {router} = this.props;
    router.push('/credential');
  }

  render() {
    return (
      <div>
        <h2 className='content-heading'>Terms and Conditions</h2>
        <Divider className='content-divider'/>
        <TermButton
          onTermEdit={this.handleTermEdit}
          onAccountEdit={this.handleAccountEdit} />
        <TermDisplay
          term={this.props.data}/>
      </div>
    );
  }
}

TermComponent.displayName = 'TermTermComponent';

// Uncomment properties you need
// TermComponent.propTypes = {};
// TermComponent.defaultProps = {};

export default TermComponent;
