/**
 * Created by jonna on 1/5/18.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';

import TermForm from './TermForm';

class TermEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      term: {
        name: '',
        description: '',
      },
      uploadImage: {},
      imageModule: Config.IMAGE.PROFILE,
    }
  }

  componentWillMount() {
    if (_.isEmpty(this.props.termState.records)) {
      let action = this.props.actions;
      action.getTerm();
    }
    else {
      this.setState({
        term: this.props.termState.records
      });
    }
  }

  componentWillReceiveProps(nextProps){
    if (!_.isEmpty(nextProps.termState.records)) {
      this.setState({
        term: nextProps.termState.records
      });
    }
  }

  // handle going back to term
  handleReturn = () => {
    this.props.router.push('/term');
  }

  // handle Update
  handleUpdate = () => {
    if (this.validateInput(this.state.term)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_PROFILE_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateTerm(this.state.term, this.state.uploadImage, this.props.router);
          }
        });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    console.log('data', data);
    if (data.name == '') {
      dialogActions.openNotification('Oops! No title found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let term = this.state.term;
    term[field] = e.target.value;
  }

  // handle image change
  handleImageChange = (e) => {
    this.setState({ uploadImage: e });
  }

  render(){
    console.info('TermEditor', this.props.termState);
    return(
      <div>
        <h2 className='content-heading'> Term </h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        <UpdateButton handleOpen={this.handleUpdate}/>

        <TermForm
          data={this.props.termState}
          shouldEdit={true}
          onChange={this.handleData}
          onImageChange={this.handleImageChange}
          imageModule={this.state.imageModule}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    termState: state.termState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    termAction: require('../../actions/termAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.termAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(TermEditor);
