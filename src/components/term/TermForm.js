/**
 * Created by jonna on 1/5/18.
 */
import React from 'react';
import Config from '../../config/base';
import { TextAreaField } from '../common/TextComponent';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';
import {CleanWord} from '../common/Utility';

class TermForm extends React.Component{

  state = {
    shouldDisplay: false,
  };

  componentWillMount(){
    if (!_.isEmpty(this.props.data.records)){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.records)) {
        this.setState({shouldDisplay: true});
      }
    }
  }


  render(){
    const {data} = this.props;
    console.info('TermForm', data);
    console.info('shouldDisplay', this.state.shouldDisplay);

    const divStyle = {
      width: '100%'
    };

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={12} lg={12}>
              <div className='pull-left content-field-holder'>
                <TextField
                  name="name"
                  hintText="Enter Title"
                  floatingLabelText="Title"
                  defaultValue={(data.records.name) ? data.records.name : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextAreaField
                  name="description"
                  hintText="Enter Description"
                  floatingLabelText="Description"
                  defaultValue={(data.records.description) ? CleanWord(data.records.description) : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  full={true}
                  /><br />


              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default TermForm;
