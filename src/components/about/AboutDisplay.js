/**
 * Created by jonna on 1/4/18.
 */
import React from 'react';
import Config from '../../config/base';
import { Grid, Row, Col } from 'react-bootstrap';

import HttpIcon from 'material-ui/svg-icons/file/cloud';
import EmailIcon from 'material-ui/svg-icons/communication/email';
import PhoneIcon from 'material-ui/svg-icons/hardware/smartphone';
import ContactIcon from 'material-ui/svg-icons/communication/phone';
import Address from 'material-ui/svg-icons/communication/location-on';
import Person from 'material-ui/svg-icons/action/face';


require('styles/profile.css');

class AboutDisplay extends React.Component {
  createMarkup = () => {
    return {__html: 'First &middot; Second'};
  }

  render(){
    if (this.props.about.status == 'Success') {
      const {records} = this.props.about;

      return(
        <div className='content-container'>
          <Grid fluid>
            <Row>
              <Col xs={12} sm={12} md={12} lg={12} >
                <div>
                  <h1 className='profile-label-company'>{records.name} </h1>
                  <div className='profile-info' dangerouslySetInnerHTML={{'__html':records.description}} ></div>
                </div>
              </Col>
            </Row>
          </Grid>
        </div>
      );
    }
    else{
      return null;
    }
  }
}

export default AboutDisplay;
