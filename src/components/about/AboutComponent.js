/**
 * Created by jonna on 1/4/18.
 */
'use strict';
/* Core Components */
import React from 'react';
import { AboutButton } from '../common/buttons';
import Divider from 'material-ui/Divider';
import AboutDisplay from './AboutDisplay';
import jwtDecode from 'jwt-decode';
import Config from '../../config/base';

class AboutComponent extends React.Component {

  componentWillMount() {
    let userToken = jwtDecode(sessionStorage.getItem(Config.MERCHANT_NAME));
    if (userToken.cmsTabs.split(',')[0] == 'order') {
      this.props.router.push('/inventory');
      return;
    }

    let action = this.props.actions;
    action.getAbout();
  }

  onAboutChange = (e) => {
    const field = e.target.name;
    const about = this.state.data;
    about[field] = e.target.value;
    return this.setState({data: about});
  }

  handleAboutEdit = () => {
    const {router} = this.props;
    router.push('/about_edit');
    console.log("Handle Edit");
  }

  handleAccountEdit = () => {
    const {router} = this.props;
    router.push('/credential');
  }

  render() {
    return (
      <div>
        <h2 className='content-heading'>About Us</h2>
        <Divider className='content-divider'/>
        <AboutButton
          onAboutEdit={this.handleAboutEdit}
          onAccountEdit={this.handleAccountEdit} />
        <AboutDisplay
          about={this.props.data}/>
      </div>
    );
  }
}

AboutComponent.displayName = 'AboutAboutComponent';

// Uncomment properties you need
// AboutComponent.propTypes = {};
// AboutComponent.defaultProps = {};

export default AboutComponent;
