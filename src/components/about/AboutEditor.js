/**
 * Created by jonna on 1/4/18.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';

import AboutForm from './AboutForm';

class AboutEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      about: {
        name: '',
        description: '',
      }
    }
  }

  componentWillMount() {
    if (_.isEmpty(this.props.aboutState.records)) {
      let action = this.props.actions;
      action.getAbout();
    }
    else {
      this.setState({
        about: this.props.aboutState.records
      });
    }
  }

  componentWillReceiveProps(nextProps){
    if (!_.isEmpty(nextProps.aboutState.records)) {
      this.setState({
        about: nextProps.aboutState.records
      });
    }
  }

  // handle going back to about
  handleReturn = () => {
    this.props.router.push('/about');
  }

  // handle Update
  handleUpdate = () => {
    if (this.validateInput(this.state.about)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_ABOUT_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateAbout(this.state.about, this.state.uploadImage, this.props.router);
          }
        });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    console.log('data', data);
    if (data.name == '') {
      dialogActions.openNotification('Oops! No title found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let about = this.state.about;
    about[field] = e.target.value;
  }

  // handle image change
  handleImageChange = (e) => {
    this.setState({ uploadImage: e });
  }

  render(){
    console.info('AboutEditor', this.props.aboutState);
    return(
      <div>
        <h2 className='content-heading'> About </h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        <UpdateButton handleOpen={this.handleUpdate}/>

        <AboutForm
          data={this.props.aboutState}
          shouldEdit={true}
          onChange={this.handleData}
          onImageChange={this.handleImageChange}
          imageModule={this.state.imageModule}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    aboutState: state.aboutState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    aboutAction: require('../../actions/aboutAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.aboutAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(AboutEditor);
