/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import Config from '../../config/base';
import { TextAreaField } from '../common/TextComponent';
import { DefaultTextArea, LabelListField } from '../common/fields';
import {ListDropChips, ListDrop} from '../common/lists';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import DatePicker from 'material-ui/DatePicker';
import MenuItem from 'material-ui/MenuItem';
import ImageUpload from '../common/ImageUpload';
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';


class PackageForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      shouldDisplay: false,
      selectedGroup: '',
      groupNames: [{id:1, name: 'Pizza with Soda'}, {id:2, name: '4 Pizza with Soda'}]
    };
  }

  componentWillMount(){
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({
          shouldDisplay: true,
          selectedGroup: this.state.groupNames.filter(
            x=>{return x.name == nextProps.data.selectedRecord.group}
          )[0].id
        });
      }
    }
  }

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  handleGroupChange = (selectedGroup) => {
    console.info('handlePackageChange', selectedGroup);
    if (selectedGroup) {
      this.setState({selectedGroup});
    }
    else{
      this.setState({selectedGroup: ''});
    }
    let _groupname = this.state.groupNames.filter( x=> { return x.id == selectedGroup})[0].name;
    this.props.onGroupChange(_groupname);
  }

  render(){
    const {data} = this.props;

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <Col xs={6} sm={6} md={6} lg={6}>
              <div className='content-container'>
                <div className='item-image-box'>
                  <ImageUpload
                    onImageChange={this.props.onImageChange}
                    image={(data.selectedRecord.image) ? data.selectedRecord.image : '' }
                    imageModule={this.props.imageModule}/>
                </div>
              </div>
            </Col>
            <Col xs={6} sm={6} md={6} lg={6}>
              <div className='pull-left content-field-holder'>
                <LabelListField placeholder='Group'>
                  <ListDrop
                    onChange={this.handleGroupChange}
                    placeholder="Select Group"
                    data={this.state.groupNames}
                    defaultValue={this.state.selectedGroup}
                    defaultKey="id"
                  />
                </LabelListField>
                <TextField
                  disabled={(this.props.shouldEdit) ? true: false}
                  name="productCode"
                  hintText="Enter Product Code"
                  floatingLabelText="Product Code"
                  defaultValue={(data.selectedRecord.productCode) ? data.selectedRecord.productCode : '' }
                  onChange={this.props.onChange}
                /><br />
                <TextField
                  name="name"
                  hintText="Enter Name"
                  floatingLabelText="Name"
                  defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                  onChange={this.props.onChange}
                /><br />
                <DefaultTextArea
                  name="description"
                  hintText="Enter Description"
                  floatingLabelText="Description"
                  defaultValue={(data.selectedRecord.description) ? data.selectedRecord.description : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  regular={true}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  /><br />

                <LabelListField placeholder='Pizzas'>
                  <ListDropChips
                    show={true}
                    onChange={this.props.onPizzaChange}
                    placeholder="Select Pizza(s)"
                    data={this.props.data.products}
                    defaultValue={data.selectedRecord.pizzaID}
                    defaultKey="prodID"
                    identity="pizzas"
                  />
                </LabelListField>
                <LabelListField placeholder='Drinks'>
                  <ListDropChips
                    show={true}
                    onChange={this.props.onDrinkChange}
                    placeholder="Select Drink(s)"
                    data={this.props.data.drinks}
                    defaultValue={data.selectedRecord.drinks}
                    defaultKey="drinkID"
                    identity="drinks"
                  />
                </LabelListField>

                <TextField
                  name="drinkqty"
                  type="number"
                  hintText="Enter Allowed Drink Quantity"
                  floatingLabelText="Allowed Drink Quantity"
                  defaultValue={(data.selectedRecord.drinkqty) ? data.selectedRecord.drinkqty : '' }
                  onChange={this.props.onChange}
                />
                <TextField
                  name="price"
                  type="number"
                  hintText="Enter Price"
                  floatingLabelText="Price"
                  defaultValue={(data.selectedRecord.price) ? data.selectedRecord.price : '' }
                  onChange={this.props.onChange}
                /><br />
                <TextField
                  name="order"
                  type="number"
                  hintText="Enter Order"
                  floatingLabelText="Order"
                  defaultValue={(data.selectedRecord.order) ? data.selectedRecord.order : '' }
                  onChange={this.props.onChange}
                /><br />
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default PackageForm;
