/**
 * Created by jedachas on 3/9/17.
 */
 import React, {Component} from 'react';
 import ListTable from '../common/table/ListTableComponent';
 import Config from '../../config/base';

 import GridRow from './PackageGridRow';

class PackageList extends Component {
  handleGetData = (params) => {
    this.props.actions.getPackages(params);
  }

  render(){
    let {packages} = this.props.data;
    const headers = [
      { title: '', value: ''},
      { title: 'Name', value: 'name'},
      { title: 'Group', value: 'group'},
      { title: 'Price', value: 'price'},
      { title: 'Status', value: 'status'}
    ];

    var rows = [];
    if (packages.records.length > 0) {
      packages.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}
          module={Config.IMAGE.PACKAGE}/>);
      });
    }

    return(
      <div className='content-container'>
        <ListTable
          headers={headers}
          data={this.props.data.packages}
          onGetData={this.handleGetData}>
            {rows}
        </ListTable>
      </div>
    );
  }

}

export default PackageList;
