/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton, AddButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';

import PackageForm from './PackageForm';

class PackageEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      package: {
        categoryID: 'CATuc14244iEYNwz',
        packageCode: '',
        name: '',
        description: '',
        drinks: [],
        drinkqty: 1,
        pizzaID: [],
        unit: 7,
        price: 0,
        order: 0,
        status: ''
      },
      uploadImage: {},
      imageModule: Config.IMAGE.PACKAGE,
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewPackage(this.props.params.id);
    }
    actions.getPackageDrinks();
    actions.getPackageProducts();
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.packageState.selectedRecord)) {
        this.setState({
          package: nextProps.packageState.selectedRecord
        });
      }
    }
  }

  // handle going back to Package
  handleReturn = () => {
    this.props.router.push('/package');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    if (this.validateInput(this.state.package)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_PACKAGE_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updatePackage(this.state.package, this.state.uploadImage, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    console.info('handleAdd:state', this.state);
    if (this.validateInput(this.state.package)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_PACKAGE_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addPackage(this.state.package, this.state.uploadImage, this.props.router);
          }
      });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let packagefield = this.state.package;
    packagefield[field] = e.target.value;
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.package['status'] = e;
  }

  handleGroupChange = (value) => {
    this.state.package['group'] = value;
  }

  // handle image change
  handleImageChange = (e) => {
    this.setState({ uploadImage: e });
  }

  handleDrinkChange = (val) => {
    this.state.package['drinks'] = val.map(v=> {return v.id}).join();
  }

  handlePizzaChange = (val) => {
    this.state.package['pizzaID'] = val.map(v=> {return v.id}).join();
  }

  render(){
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Package' : 'Add Package'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <AddButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <PackageForm
          data={this.props.packageState}
          shouldEdit={ (this.props.params.id) ? true : false }
          onChange={this.handleData}
          onGroupChange={this.handleGroupChange}
          onImageChange={this.handleImageChange}
          imageModule={this.state.imageModule}
          onStatusChange={this.handleStatusChange}
          onDrinkChange={this.handleDrinkChange}
          onPizzaChange={this.handlePizzaChange}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    packageState: state.packageState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    packageAction: require('../../actions/packageAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.packageAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(PackageEditor);
