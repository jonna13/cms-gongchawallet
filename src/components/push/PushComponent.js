'use strict';

import React from 'react';
import Config from '../../config/base';
import { TextAreaField } from '../common/TextComponent';
import Divider from 'material-ui/Divider';
import TextField from 'material-ui/TextField';
import {SendButton} from '../common/buttons';

class PushComponent extends React.Component {
  state = {
    push: {
      message: '',
      type: 'normal'
    }
  }

  onChange = (event) => {
    const field = event.target.name;
    const details = this.state.push;
    details[field] = event.target.value;
    this.setState({ push: { message: event.target.value } });
  }

  handleSend = () => {
    console.info('PushComponent', this.state);
    if (this.validateInput(this.state.push)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_CREDENTIAL_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.sendPush(this.state.push);
            this.setState({ push: { message: '' } });
          }
        });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (data.message == '') {
      dialogActions.openNotification('Oops! No message found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    return true;
  }

  render() {
    return (
      <div>
        <h2 className='content-heading'>Push Notification</h2>
        <Divider className='content-divider' />
        { /* Action Buttons */ }
        <SendButton
            handleOpen={this.handleSend}/>

        <div className='pull-left content-field-holder'>
          <TextAreaField
            name="message"
            hintText="Enter Message"
            floatingLabelText="Message"
            onChange={this.onChange}
            defaultValue={this.state.push.message}
            rows={5}
            wide={true}
            maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
            /><br />
        </div>
      </div>
    );
  }
}

PushComponent.displayName = 'PushPushComponent';

// Uncomment properties you need
// PushComponent.propTypes = {};
// PushComponent.defaultProps = {};

export default PushComponent;
