/**
 * Created by jedachas on 3/9/17.
 */
import React, {Component} from 'react';
import ListTable from '../common/table/ListTableComponent';
import Config from '../../config/base';
import GridRow from './InventoryGridRow';
import jwtDecode from 'jwt-decode';

class InventoryList extends Component {
  handleGetData = (params) => {
    let userToken = jwtDecode(sessionStorage.getItem(Config.MERCHANT_NAME));
    params.selectedLocation = userToken.locID;
    this.props.actions.getInventorys(params);
  }

  render(){
    let {inventorys} = this.props.data;
    const headers = [
      { title: '', value: ''},
      { title: 'Branch', value: 'locName'},
      { title: 'Product code', value: 'productCode'},
      { title: 'Product', value: 'productName'},
      { title: 'Quantity', value: 'quantity'},
      { title: 'Status', value: 'status'}
    ];

    var rows = [];
    if (inventorys.records.length > 0) {
      inventorys.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}/>);
      });
    }

    return(
      <div className='content-container'>

        <ListTable
          headers={headers}
          data={this.props.data.inventorys}
          onGetData={this.handleGetData}>
            {rows}
        </ListTable>
      </div>
    );
  }

}

export default InventoryList;
