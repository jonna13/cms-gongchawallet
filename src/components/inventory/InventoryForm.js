/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import Config from '../../config/base';
import { TextAreaField } from '../common/TextComponent';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import DatePicker from 'material-ui/DatePicker';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import {ListDropChips, ListDropMultiChips, ListDrop} from '../common/lists';
import AppStyle from '../../styles/Style.js';


class InventoryForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      location: '',
      shouldDisplay: false,
    };
  }

  componentWillMount(){
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({
          shouldDisplay: true
        });
      }
    }
  }

  handleLocationChange = (location) => {
    if (location) {
      this.setState({location});
      this.props.onLocationChange(location);
    }
  }

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  render(){
    const {data} = this.props;
    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                {/* Edit */}
                <ListDrop
                  show={(!this.props.shouldEdit) ? false: true}
                  placeholder="Select Location"
                  data={this.props.data.locations}
                  defaultValue={data.selectedRecord.locID}
                  defaultKey="locID"
                  disabled={true}
                />
                <ListDrop
                  show={(!this.props.shouldEdit) ? false: true}
                  placeholder="Select Product"
                  data={this.props.data.products}
                  defaultValue={data.selectedRecord.prodID}
                  defaultKey="prodID"
                  disabled={true}
                />
                {/* Add */}
                <ListDropMultiChips
                  show={(this.props.shouldEdit) ? false: true}
                  onChange={this.props.onLocationChange}
                  placeholder="Select Locations"
                  data={this.props.data.locations}
                  defaultValue={data.selectedRecord.locations}
                  defaultKey="locID"
                  identity="location"
                />
                <ListDropMultiChips
                  show={(this.props.shouldEdit) ? false: true}
                  onChange={this.props.onProductChange}
                  placeholder="Select Products"
                  data={this.props.data.products}
                  defaultValue={data.selectedRecord.products}
                  defaultKey="prodID"
                  SecondaryKey="productCode"
                  identity="product"
                />
              </div>
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                <TextField
                  name="quantity"
                  type="number"
                  hintText="Enter Quantity"
                  floatingLabelText="Quantity"
                  defaultValue={(data.selectedRecord.quantity) ? data.selectedRecord.quantity : '' }
                  onChange={this.props.onChange}
                /><br />
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : true }
                  onCheck={this.onCheck}
                  disabled={true}
                />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default InventoryForm;
