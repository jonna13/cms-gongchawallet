'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import Config from '../../config/base';
import {AddButton} from '../common/buttons';
import {SearchBar,CheckStatus,PageSize} from '../common/widgets';
import jwtDecode from 'jwt-decode';

import InventoryList from './InventoryList';

class InventoryComponent extends React.Component {

  handleAdd = () => {
    const {router} = this.props;
    router.push('/inventory_add');
  }

  handleEdit = (item) => {
    console.info('edit item', item);
    const {router} = this.props;
    router.push('/inventory/'+item.inventoryID);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.inventorys;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.inventorys;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (text) => {
    let {pageinfo} = this.props.data.inventorys;
    pageinfo.searchFilter = text;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.inventorys;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.inventorys;
    let userToken = jwtDecode(sessionStorage.getItem(Config.MERCHANT_NAME));
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus,
      selectedLocation: userToken.locID
    }
    actions.getInventorys(params);
  }

  render() {
    const {pageinfo} = this.props.data.inventorys;
    const {data} = this.props;
    let userToken = jwtDecode(sessionStorage.getItem(Config.MERCHANT_NAME));

    return (
      <div>
        <h2 className='content-heading'>Inventory</h2>
        <h5 className='content-record-label'>Records: <b>{ pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        { (userToken.cmsTabs.split(',')[0] == 'order') ? '' : <AddButton handleOpen={this.handleAdd}/>}
        <CheckStatus
            pageinfo={data.inventorys.pageinfo}
            handleChange={this.handleStatusChange}/>
        <PageSize
            pageinfo={data.inventorys.pageinfo}
            handleChange={this.handlePageSize}/>
        <SearchBar
            pageinfo={data.inventorys.pageinfo}
            handleChange={this.handleSearchChange}
            handleClose={this.handleSearchClose}/>

        { /* List  */ }
        <InventoryList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit} />
      </div>
    );
  }
}

export default InventoryComponent;
