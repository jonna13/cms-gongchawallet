/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton, AddButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';
import qs from 'qs';

import InventoryForm from './InventoryForm';

class InventoryEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      inventory: {
        locID: [],
        prodID: [],
        prodCode: [],
        quantity: 0,
        status: 'active'
      }
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewInventory(this.props.params.id);
    }
    actions.getReferences();
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.inventoryState.selectedRecord)) {
        this.setState({
          inventory: nextProps.inventoryState.selectedRecord
        });
      }
    }
  }

  // handle going back to Inventory
  handleReturn = () => {
    this.props.router.push('/inventory');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    if (this.validateInput(this.state.inventory)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_INVENTORY_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateInventory(this.state.inventory, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.inventory)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_INVENTORY_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addInventory(this.state.inventory, this.props.router);
          }
      });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (data.quantity == 0) {
      dialogActions.openNotification('Oops! No quantity found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (_.isEmpty(data.locID)) {
      dialogActions.openNotification('Oops! No locations selected', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (_.isEmpty(data.prodID)) {
      dialogActions.openNotification('Oops! No products selected', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let inventory = this.state.inventory;
    inventory[field] = e.target.value;
  }

  handleLocationChange = (val) => {
    this.state.inventory['locID'] = JSON.stringify(val);
  }

  handleProductChange = (val) => {
    this.state.inventory['prodID'] =  JSON.stringify(val);
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.inventory['status'] = e;
  }

  render(){
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Inventory' : 'Add Inventory'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <AddButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <InventoryForm
          data={this.props.inventoryState}
          shouldEdit={ (this.props.params.id) ? true : false }
          onChange={this.handleData}
          onLocationChange={this.handleLocationChange}
          onProductChange={this.handleProductChange}
          onStatusChange={this.handleStatusChange}
          imageModule={this.state.imageModule}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    inventoryState: state.inventoryState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    inventoryAction: require('../../actions/inventoryAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.inventoryAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(InventoryEditor);
