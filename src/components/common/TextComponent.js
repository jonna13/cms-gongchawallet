'use strict';

import React from 'react';
import TextField from 'material-ui/TextField';
import {CleanWord} from './Utility';
import classNames from 'classnames';

export class TextAreaField extends React.Component {

  state = {
    remaining: 0
  }

  handleChange = (e) => {
    this.props.onChange(e);
    this.setState({text: e.target.value });
    let chars = e.target.value;
    this.setState({remaining: this.props.maxSize - chars.length })
  }

  componentWillMount(){
    this.setState({ remaining: (this.props.defaultValue.length > 0) ?
      (this.props.maxSize - this.props.defaultValue.length) : this.props.maxSize });
  }

  render() {
    let areaHolderClass = classNames({
      'text-area-holder-default': (!this.props.wide && !this.props.regular),
      'text-area-holder-regular': this.props.regular,
      'text-area-holder-wide': this.props.wide,
      'text-area-content-full': this.props.full,
    });
    let textareaClass = classNames({
      'text-area-content-wide': this.props.wide,
      'text-area-content-regular': this.props.regular,
      'text-area-content-full': this.props.full
    });

    return (
      <div className={areaHolderClass}>
        <TextField
          name={this.props.name}
          hintText={this.props.hintText}
          floatingLabelText={this.props.floatingLabelText}
          defaultValue={CleanWord(this.props.defaultValue)}
          onChange={this.handleChange}
          multiLine={true}
          rows={this.props.rows}
          className={textareaClass}
          disabled={this.props.disabled}
        /><br />
        <span className="text-remaining">
          Remaining: {this.state.remaining}
        </span>
      </div>
    );
  }
}


export class TextAreaValuField extends React.Component {

  state = {
    remaining: 0
  }

  handleChange = (e) => {
    this.props.onChange(e);
    this.setState({text: e.target.value });
    let chars = e.target.value;
    this.setState({remaining: this.props.maxSize - chars.length })
  }

  componentWillMount(){
    this.setState({ remaining: (this.props.value.length > 0) ?
      (this.props.maxSize - this.props.value.length) : this.props.maxSize });
  }

  componentWillReceiveProps(nextProps){
    this.setState({ remaining: (nextProps.value.length > 0) ?
      (nextProps.maxSize - nextProps.value.length) : nextProps.maxSize });
  }

  render() {
    let areaHolderClass = classNames({
      'text-area-holder-wide': this.props.wide,
      'text-area-holder-regular': this.props.regular
    });
    let textareaClass = classNames({
      'text-area-content-wide': this.props.wide,
      'text-area-content-regular': this.props.regular
    });

    return (
      <div className={areaHolderClass}>
        <TextField
          name={this.props.name}
          hintText={this.props.hintText}
          floatingLabelText={this.props.floatingLabelText}
          value={CleanWord(this.props.value)}
          onChange={this.handleChange}
          multiLine={true}
          rows={this.props.rows}
          className={textareaClass}
        /><br />
        <span className="text-remaining">
          Remaining: {this.state.remaining}
        </span>
      </div>
    );
  }
}
