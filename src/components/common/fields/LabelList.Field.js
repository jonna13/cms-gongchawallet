/**
 * Created by jedachas on 3/30/17.
 */
'use strict';

import React, {Component} from 'react';

class LabelListField extends Component {
  render() {
    return (
      <div className={(!this.props.show) ? 'hide':''}>
        <label className='label-list-placeholder'>{this.props.placeholder}</label>
        {this.props.children}
      </div>
    );
  }
}

LabelListField.defaultProps = {
  show: true
};

export default LabelListField;
