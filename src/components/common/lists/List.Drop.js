'use strict';

import React, {
  Component
} from 'react';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem'
import _ from 'lodash';

class ListDrop extends Component {
  state = {
    value: ''
  }

  handleDataChange = (e, idx, val) => {
    console.log('handleDataChange', val);
    this.setState({value: val});
    this.props.onChange(val);
  }

  componentWillMount(){
    this.setState({value: (this.props.defaultValue) ? this.props.defaultValue : ''});
  }

  componentWillReceiveProps(nextState){
    if (nextState.defaultValue) {
      this.setState({value: nextState.defaultValue});
    }
  }

  render(){
    let list = [];
    if (this.props.data) {
      list.push(<MenuItem key={0} value='' primaryText={this.props.placeholder} />);
      this.props.data.map( (val, idx) => {
        list.push(<MenuItem key={(idx+1)} value={val[this.props.defaultKey]} primaryText={val[this.props.primaryText]} />);
      });
    }

    return(
      <div className={(this.props.show) ? 'visible' : 'hide'}>
        <DropDownMenu
          className='dropdownButton'
          value={this.state.value}
          onChange={this.handleDataChange}
          disabled={this.props.disabled}
          autoWidth={false} >
          {list}
        </DropDownMenu>
      </div>
    );
  }
}

ListDrop.defaultProps = {
  disabled: false,
  show: true,
  primaryText: 'name'
};

export default ListDrop;
