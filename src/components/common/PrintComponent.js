'use strict';

import React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import Print from 'material-ui/svg-icons/action/print';

class PrintComponent extends Component {

  render(){
    return(
      <div className="printcode-container">
        <IconButton onClick={this.props.onPrint}>
          <Print className="icon-action-active"/>
        </IconButton>
      </div>
    );
  }
}

export default PrintComponent;
