'use strict';

import React, {
  Component,
  PropTypes
} from 'react';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem'
import Chip from 'material-ui/Chip';
import _ from 'lodash';

export class ListDrop extends Component {
  state = {
    value: ''
  }

  handleDataChange = (e, idx, val) => {
    this.setState({value: val});
    this.props.onChange(val);
  }

  componentWillMount(){
    if (this.props.defaultValue) {
      this.setState({value: this.props.defaultValue});
    }
  }

  componentWillReceiveProps(nextState){
    if (nextState.defaultValue) {
      this.setState({value: nextState.defaultValue});
    }
  }

  render(){
    let list = [];
    if (this.props.data) {
      list.push(<MenuItem key={0} value='' primaryText={this.props.placeholder} />);
      this.props.data.map( (val, idx) => {
        list.push(<MenuItem key={(idx+1)} value={val[this.props.defaultKey]} primaryText={val.name} />);
      });
    }

    return(
      <DropDownMenu value={this.state.value} onChange={this.handleDataChange}
        autoWidth={false} className='dropdownButton'>
        {list}
      </DropDownMenu>
    );
  }
}

ListDrop.propTypes = {
  data: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  defaultKey: PropTypes.string,
  defaultValue: PropTypes.string,
  placeholder: PropTypes.string
}

export class ListDropChips extends Component{
  state = {
    value: '',
    dataArray: []
  }

  handleDataChange = (e, idx, val) => {
    if (val) {
      let name = this.props.data.filter( x =>{return x[this.props.defaultKey] == val})[0].name;
      let value = this.props.data.filter( x =>{return x[this.props.defaultKey] == val;})[0][this.props.defaultKey];
      let dataArray = this.state.dataArray;
      if (val == 'all'){
        dataArray = [];
        dataArray.push({key: idx, name: name, value: value });
        this.setState({dataArray: dataArray});
      }
      else{
        if ( _.isEmpty(this.state.dataArray.filter(x=>{return x.value == val})) ) {
            if ( !_.isEmpty( this.state.dataArray.filter(x=>{ return x.value == 'all' }) ) ) {
              dataArray = [];
            }
            dataArray.push({key: idx, name: name, value: value });
            this.setState({ dataArray: dataArray });
          }
      }
      this.props.onChange(dataArray);
    }
    this.setState({value: val});
  }

  componentWillMount(){
    if (!_.isEmpty(this.props.defaultValue) && !_.isEmpty(this.props.data)) {
      let val = this.props.defaultValue.split(',').map( (v,idx)=> {
        let vx = this.props.data.filter( x => {
            return x[this.props.defaultKey] == v;
          })[0];
        return {key: idx, name: vx.name, value:  vx[this.props.defaultKey] };
      });
      this.setState({dataArray: val});
    }
  }

  componentWillReceiveProps(nextState){
    this.setState({value: ''});
    if (nextState.remove) {
      this.setState({dataArray: []});
    }
    if (!_.isEmpty(nextState.defaultValue) && !_.isEmpty(nextState.data)) {
      let val = nextState.defaultValue.split(',').map( (v,idx)=> {
        let vx = nextState.data.filter( x => {
            return x[nextState.defaultKey] == v;
          })[0];
        return {key: idx, name: vx.name, value:  vx[this.props.defaultKey] };
      });
      this.setState({dataArray: val});
    }
  }

  handleChipRemove = (key) => {
    const chipToDelete = this.state.dataArray.map((chip) => chip.key).indexOf(key);
    this.state.dataArray.splice(chipToDelete, 1);
    this.setState({dataArray: this.state.dataArray});
    this.props.onChange(this.state.dataArray);
  }

  renderChips(data) {
    return (
      <Chip
        className='chip-data'
        key={data.key}
        onRequestDelete={() => this.handleChipRemove(data.key)}
      > {data.name} </Chip>
    );
  }

  render(){
    let list = [];
    if (this.props.data) {
      list.push(<MenuItem key={0} value='' primaryText={this.props.placeholder} />);
      this.props.data.map( (val, idx) => {
        list.push(<MenuItem key={(idx+1)} value={val[this.props.defaultKey]} primaryText={val.name} />);
      });
    }

    return(
      <div className={(this.props.show) ? 'visible' : 'hide'}>
        <DropDownMenu value={this.state.value} onChange={this.handleDataChange}
          autoWidth={false} className='dropdownButton'>
          {list}
        </DropDownMenu>
        <div className='chip-holder'> {this.state.dataArray.map(this.renderChips, this)} </div>
      </div>
    );
  }
}

ListDropChips.propTypes = {
  data: React.PropTypes.array.isRequired,
  onChange: React.PropTypes.func.isRequired,
  defaultKey: React.PropTypes.string,
  defaultValue: React.PropTypes.string,
  show: React.PropTypes.bool,
  remove: React.PropTypes.bool,
  placeholder: React.PropTypes.string
}

ListDropChips.defaultProps = {
  show: true,
  remove: false
};
