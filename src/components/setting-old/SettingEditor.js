/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';

import SettingForm from './SettingForm';

class SettingEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      setting: {
        name: '',
        camount: 0,
        cpoints: 0,
        description: '',
        icon: '',
        brandID: '',
        brandName: ''
      }
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewSetting(this.props.params.id);
    }
    // actions.getBrands();
    // actions.getBrands2();
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.settingState.selectedRecord)) {
        this.setState({
          setting: nextProps.settingState.selectedRecord
        });
      }
    }
  }

  // handle going back to Setting
  handleReturn = () => {
    this.props.router.push('/setting');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    const {dialogActions} = this.props;
    if (this.validateInput(this.state.setting)) {
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_SETTING_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateSetting(this.state.setting, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.setting)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_SETTING_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            console.log('Setting Action', actions);
            actions.addSetting(this.state.setting, this.props.router);
          }
      });
    }
  }

  validateInput = (data) => {
    console.log('validateData settings', data);
    const {dialogActions} = this.props;
    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.camount == 0) {
      dialogActions.openNotification('Oops! No amount found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.cpoints == 0) {
      dialogActions.openNotification('Oops! No points found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.brandID == '') {
      dialogActions.openNotification('Oops! No brandID found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let setting = this.state.setting;
    setting[field] = e.target.value;
    console.log('HandleData', setting[field]);
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.setting['status'] = e;
  }

  // handle image change
  handleImageChange = (e) => {
    this.setState({ uploadImage: e });
  }

  // handle brand
  handleBrandChange = (value, name) => {
    this.state.setting['brandID'] = value;
    this.state.setting['name'] = name;
  }

  // handle brand2
  handleBrandChange2 = (value) => {
    this.state.setting['brandID'] = value;
  }

  render(){

    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Earn Settings' : 'Add Setting'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <SettingForm
          data={this.props.settingState}
          actions={this.props.actions}
          shouldEdit={ (this.props.params.id) ? true : false }
          onChange={this.handleData}
          onBrandChange={this.handleBrandChange}
          onBrandChange2={this.handleBrandChange2}
          onStatusChange={this.handleStatusChange}
          onImageChange={this.handleImageChange}
          imageModule={this.state.imageModule}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    settingState: state.settingState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    settingAction: require('../../actions/settingAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.settingAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingEditor);
