/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import Config from '../../config/base';
import { DefaultTextArea } from '../common/fields';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import DatePicker from 'material-ui/DatePicker';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';


class SettingForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      camount: 0,
      cpoints: 0,
      description: '',
      icon: '',
      brandID: '',
      brandName: '',
      shouldDisplay: false,
    };
  }

  componentWillMount(){
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      this.setState({
        brandID: nextProps.data.selectedRecord.brandID,
        brandName: nextProps.data.selectedRecord.brandName,
      });
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({shouldDisplay: true});
      }
    }
  }

  handleBrandChange = (e, idx, brandID) => {
    const {actions} = this.props;
    let name = this.props.data.brands.filter(x=> { return x.brandID == brandID})[0].name;
    this.setState({brandID});
    this.props.onBrandChange(brandID, name);
  };

  handleBrandChange2 = (e, idx, brandID) => {
    this.setState({brandID});
    this.props.onBrandChange2(brandID);
    actions.getBrands(brandID);
  };

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  render(){
    let brandList = [];
    let brandList2 = [];
    const {data} = this.props;
    console.log('Render data', this.props.data.brands2);

    if (this.props.data.brands) {
      brandList.push(<MenuItem key={0} value='Select Brand' />);
      this.props.data.brands.map( (val, idx) => {
        brandList.push(<MenuItem key={(idx+1)} value={val.brandID} primaryText={val.name} />);
      });
    }
    if (this.props.data.brands2) {
      brandList2.push(<MenuItem key={0} value='Select Brand' />);
      this.props.data.brands2.map( (val, idx) => {
        brandList2.push(<MenuItem key={(idx+1)} value={val.brandID} primaryText={val.name} />);
      });
    }

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                <br />
                {/* (this.props.shouldEdit) ?
                  <SelectField
                    value={(data.selectedRecord.brandID) ? data.selectedRecord.brandID: this.state.brandID}
                    onChange={this.handleBrandChange2}
                    className='textfield-regular'
                    floatingLabelText="Brand"
                    hintText="Select Brand"
                    autoWidth={false}
                    disabled={true}>
                    {brandList2}
                  </SelectField>:
                  <SelectField
                    value={(data.selectedRecord.brandID) ? data.selectedRecord.brandID: this.state.brandID}
                    onChange={this.handleBrandChange}
                    className='textfield-regular'
                    floatingLabelText="Brand"
                    hintText="Select Brand"
                    autoWidth={false}>
                    {brandList}
                  </SelectField>*/}
                <TextField
                  name="name"
                  hintText="Enter Name"
                  floatingLabelText="Name"
                  defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                  disabled={true}
                /><br />
                <TextField
                  name="camount"
                  type="number"
                  hintText="Enter Amount"
                  floatingLabelText="Amount"
                  defaultValue={(data.selectedRecord.camount) ? data.selectedRecord.camount : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <TextField
                  name="cpoints"
                  hintText="Enter Points"
                  floatingLabelText="Points"
                  defaultValue={(data.selectedRecord.cpoints) ? data.selectedRecord.cpoints : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                {/*<DefaultTextArea
                  name="description"
                  hintText="Enter Description"
                  floatingLabelText="Description"
                  defaultValue={(data.selectedRecord.description) ? data.selectedRecord.description : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  regular={true}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  /><br />
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />*/}
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default SettingForm;
