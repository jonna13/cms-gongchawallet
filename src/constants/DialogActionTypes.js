/**
 * Created by jedachas on 2/9/17.
 *
 * Constant Dialog Action Types
 *
 */

export const OPEN_NOTIFICATION = 'OPEN_NOTIFICATION';
export const CLOSE_NOTIFICATION = 'CLOSE_NOTIFICATION';

export const DIALOG_OPEN_ALERT = 'DIALOG_OPEN_ALERT';
export const DIALOG_CLOSE_ALERT = 'DIALOG_CLOSE_ALERT';

export const DIALOG_OPEN_CONFIRM = 'DIALOG_OPEN_CONFIRM';
export const DIALOG_CLOSE_CONFIRM = 'DIALOG_CLOSE_CONFIRM';
