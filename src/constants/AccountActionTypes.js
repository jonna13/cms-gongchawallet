/**
 * Created by jedachas on 3/29/17.
 */
export const VIEW_ACCOUNT_SUCCESS = 'VIEW_ACCOUNT_SUCCESS';
export const GET_ACCOUNT_SUCCESS = 'GET_ACCOUNT_SUCCESS';
export const GET_ACCOUNT_LOCATION_SUCCESS = 'GET_ACCOUNT_LOCATION_SUCCESS';

export const CHANGE_FILTER_STATUS = 'CHANGE_FILTER_STATUS';
export const REMOVE_SELECTED_RECORD = 'REMOVE_SELECTED_RECORD';
