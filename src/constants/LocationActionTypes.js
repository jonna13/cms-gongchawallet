/**
 * Created by jedachas on 3/8/17.
 */
export const VIEW_LOCATION_SUCCESS = 'VIEW_LOCATION_SUCCESS';
export const GET_LOCATION_SUCCESS = 'GET_LOCATION_SUCCESS';

export const CHANGE_FILTER_STATUS = 'CHANGE_FILTER_STATUS';
export const REMOVE_SELECTED_RECORD = 'REMOVE_SELECTED_RECORD';
