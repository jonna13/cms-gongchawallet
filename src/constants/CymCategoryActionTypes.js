/**
 * Created by jedachas on 3/2/17.
 *
 * Constant Product Action Types
 *
 */
export const VIEW_CYMCATEGORY_SUCCESS = 'VIEW_CYMCATEGORY_SUCCESS';
export const VIEW_CYMCATEGORY_FAILED = 'VIEW_CYMCATEGORY_FAILED';

export const GET_CYMCATEGORY_SUCCESS = 'GET_CYMCATEGORY_SUCCESS';


export const CHANGE_FILTER_STATUS = 'CHANGE_FILTER_STATUS';
export const REMOVE_SELECTED_RECORD = 'REMOVE_SELECTED_RECORD';
