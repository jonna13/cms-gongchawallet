/**
 * Created by jedachas on 3/2/17.
 *
 * Constant Product Action Types
 *
 */
export const VIEW_CYM_SUCCESS = 'VIEW_CYM_SUCCESS';
export const VIEW_CYM_FAILED = 'VIEW_CYM_FAILED';

export const GET_CYM_SUCCESS = 'GET_CYM_SUCCESS';
export const GET_CYM_CATEGORY_SUCCESS = 'GET_CYM_CATEGORY_SUCCESS';


export const CHANGE_FILTER_STATUS = 'CHANGE_FILTER_STATUS';
export const REMOVE_SELECTED_RECORD = 'REMOVE_SELECTED_RECORD';
