import React from 'react';
import * as types from '../constants/AuthActionTypes';
import {post} from '../api/data.api';
import * as dialogAction from './dialogAction';
import * as loadingAction from './loadingAction';
import Config from '../config/base';
import jwtDecode from 'jwt-decode';

import { routerActions } from 'react-router-redux';

export const loginUser = (credentials, router) => {
  return dispatch => {

    if (credentials.username == ''){
      dispatch(dialogAction.openAlert('Invalid', 'No username'));
      return;
    }
    if (credentials.password == ''){
      dispatch(dialogAction.openAlert('Invalid', 'No password'));
      return;
    }

    dispatch(loadingAction.showLoading());
    credentials.function = 'login';
    credentials.category = Config.GATEWAY.CATEGORY;
    post(credentials).then( result => {
      let {data} = result;
      if (data.response == 'Success') {
        sessionStorage.setItem(Config.MERCHANT_NAME, data.token);
        dispatch(loginUserSuccess(data));
        dispatch(dialogAction.openNotification('Welcome ' + credentials.username,
          Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));

        let tabs = jwtDecode(data.token).cmsTabs.split(',');
        switch (tabs[0]) {
          case 'profile':
            router.push('/'); break;
          case 'push':
            router.push('/push'); break;
          case 'category':
            router.push('/category'); break;
          case 'subcategory':
            router.push('/subcategory'); break;
          case 'product':
            router.push('/product'); break;
          case 'flavor':
            router.push('/flavor'); break;
          case 'location':
            router.push('/location'); break;
          case 'account':
            router.push('/account'); break;
          case 'order':
            router.push('/inventory'); break;
          default:
            router.push('/');
        }
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else if (data.response == 'Failed'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
      }
      else if (data.response == 'Restricted'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
      }
      else {
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export const updatePassword = (credentials, router) => {
  return dispatch => {
    dispatch(loadingAction.showLoading());
    credentials.function = 'update_password';
    credentials.category = Config.GATEWAY.CATEGORY;

    post(credentials).then( result => {
      let {data} = result;
      if (data[0].response == 'Success') {
        dispatch(dialogAction.openNotification('Great! Password Updated!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
        router.push('/');
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data[0].description));
      }
      dispatch(loadingAction.hideLoading());

    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });

  }
}

export let logoutUser = (router) => {
  return dispatch => {
    dispatch(routerActions.push("/login"));
    setTimeout(function(){
      dispatch(logoutUserSuccess());
    }, 1000);
  }
}

export const logoutUserSuccess = () => {
  return{
    type: types.LOG_OUT_USER
  }
}

export const loginUserSuccess = (data) => {
  return {
    type: types.LOGIN_IN_USER,
    data
  }
}
