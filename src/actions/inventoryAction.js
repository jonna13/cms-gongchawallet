/**
 * Created by jedachas on 3/8/17.
 */
import React from 'react';
import * as types from '../constants/InventoryActionTypes';
import {get, post, upload} from '../api/data.api';
import axios from 'axios';
import * as auth from './authAction';
import * as dialogAction from './dialogAction';
import * as loadingAction from './loadingAction';
import Config from '../config/base';
import _ from 'lodash';

export let getInventorys = (config) => {
  let param = '?function=jsonlist&category='+Config.GATEWAY.CATEGORY+'&table=inventorytable&sortColumnName='+config.sortColumnName+
  '&sortOrder='+config.sortOrder+'&pageSize='+config.pageSize+
  '&currentPage='+config.currentPage+'&searchFilter='+config.searchFilter+
  '&selectedStatus='+config.selectedStatus+'&selectedLocation='+config.selectedLocation;

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadInventorySuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.INVENTORYTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.INVENTORYTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let viewInventory = (id) => {
  let param = '?function=view_record&category='+Config.GATEWAY.CATEGORY+'&table=inventorytable&filter='+id;
  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(viewInventorySuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.INVENTORYTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.INVENTORYTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

let getProducts = () => {
  let param = '?function=get_json&category='+Config.GATEWAY.CATEGORY+'&table=producttable_pizza';
  return get(param);
}

let getLocations = () => {
  let param = '?function=get_json&category='+Config.GATEWAY.CATEGORY+'&table=location';
  return get(param);
}

export let getReferences = () => {
  return dispatch => {
    axios.all([getLocations(), getProducts()])
    .then(axios.spread(function (loc, prod) {

      if (loc.data.response == 'Success') {
        dispatch(viewLocationSuccess(loc.data));
      }
      if (prod.data.response == 'Success') {
        dispatch(viewProductSuccess(prod.data));
      }

      if (loc.data.response == 'Failed') {
        let failarray = [];
        if (x.data.table == 'producttable_active') { failarray.push(Config.ERROR_CODE.DRINKTABLE); }
        if (x.data.table == 'loctable_active') { failarray.push(Config.ERROR_CODE.ADDONTABLE); }
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE,
          Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + failarray ));
      }
      else if (loc.data.response == 'Failed') {
        let emptyArray = [];
        if (x.data.table == 'producttable_active') { emptyArray.push(Config.ERROR_CODE.DRINKTABLE); }
        if (x.data.table == 'loctable_active') { emptyArray.push(Config.ERROR_CODE.ADDONTABLE); }
        dispatch(dialogAction.openNotification('Empty Data ' + emptyArray,
          Config.DIALOG_MESSAGE.NOTIFICATION_DELAY ));
      }
      else if (loc.data.response == 'Expired' || prod.data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
    }))
    .catch( (e)=> {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION));
      dispatch(loadingAction.hideLoading());
      console.info(e);
    });;
  }
}

export let addInventory = (props, router) => {
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'add_inventory';
  props.status = (props.status) ? 'active' : 'inactive';

  return dispatch => {
    dispatch(loadingAction.showLoading());
    post(props).then(result => {
      let {data} = result;
      console.info('inventory addInventory', result);
      if (data.response == 'Success') {
        dispatch(dialogAction.openNotification('New inventory added!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
        router.push('/inventory');
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_ADD_DESCRIPTION));
      }
      dispatch(loadingAction.hideLoading());

    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let updateInventory = (props, router) => {
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'update_inventory';
  if (typeof props.status == 'boolean') {
    props.status = (props.status) ? 'active' : 'inactive';
  }

  return dispatch => {
    dispatch(loadingAction.showLoading());
    post(props).then(result => {
      let {data} = result;
      console.info('updateInventory', result);
      if (data.response == 'Success') {
        dispatch(dialogAction.openNotification('Great! inventory updated', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
        dispatch(removeSelectedRecord());
        router.push('/inventory');
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_UPDATE_DESCRIPTION));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let changefilterStatus = (status) => {
  return {
    type: types.CHANGE_FILTER_STATUS,
    status: status
  }
}

export let removeSelectedRecord = () => {
  return {
    type: types.REMOVE_SELECTED_RECORD
  }
}

/*
* Type Handlers
* Success
*/
export let loadInventorySuccess = (data) => {
  return {
    type: types.GET_INVENTORY_SUCCESS,
    data
  }
}

export let viewInventorySuccess = (data) => {
  return{
    type:types.VIEW_INVENTORY_SUCCESS,
    data
  }
}

export let viewProductSuccess = (data) => {
  return{
    type:types.GET_INVENTORY_PRODUCT_SUCCESS,
    data
  }
}

export let viewLocationSuccess = (data) => {
  return{
    type:types.GET_INVENTORY_LOCATION_SUCCESS,
    data
  }
}
