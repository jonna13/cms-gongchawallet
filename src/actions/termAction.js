/**
 * Created by jonna on 1/5/18.
 *
 * Term Actions
 *
 */
import React from 'react';
import * as types from '../constants/TermActionTypes';
import {get, post, upload} from '../api/data.api';
import Config from '../config/base';
import * as auth from './authAction';
import * as dialogAction from './dialogAction';
import * as loadingAction from './loadingAction';

export let getTerm = () => {
  let param = '?function=get_json&category='+Config.GATEWAY.CATEGORY+'&table=term';

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadTermSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.TERMTABLE));
      }

      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.TERMTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let updateTerm = (props, image, router) => {
  let uploadparam = {
    image: image,
    module: Config.IMAGE.TERM
  };
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'update_term';

  return dispatch => {
    dispatch(loadingAction.showLoading());
    if (_.isEmpty(image)){
      updateSelectedRecord(props, router, dispatch);
    }
    else{
      upload(uploadparam).then(result => {
        let {data} = result;
        if (data.response == "Success") {
          props.termPic = data.data;
          updateSelectedRecord(props, router, dispatch);
        }
      }).catch( e => {
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
        dispatch(loadingAction.hideLoading());
        console.info('Error', e);
      });
    }
  }
}

let updateSelectedRecord = (props, router, dispatch) => {
  post(props).then(result => {
    let {data} = result;
    console.info('update', result);
    if (data.response == 'Success') {
      dispatch(dialogAction.openNotification('Great! Term Us updated', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
      router.push('/');
    }
    else if (data.response == 'Expired'){
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
      dispatch(auth.logoutUser());
    }
    else{
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_UPDATE_DESCRIPTION));
    }
    dispatch(loadingAction.hideLoading());
  }).catch( e => {
    dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
    dispatch(loadingAction.hideLoading());
    console.info('Error', e);
  });
}

export let updateTermSuccess = (data) => {
  return {
    type: types.UPDATE_TERM_SUCCESS,
    data
  }
}

export let loadTermSuccess = (data) => {
  return{
    type: types.GET_TERM_SUCCESS,
    data
  }
}
