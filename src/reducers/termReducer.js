/**
 * Created by jonna on 1/5/18.
 *
 * Term Reducer
 *
 */
import * as types from '../constants/TermActionTypes';
import Config from '../config/base';
import update from 'react-addons-update';

const initialState = {
  records: [],
  status: ''
};

module.exports = function(state = initialState, action) {
  /* Keep the reducer clean - do not mutate the original state. */
  //let nextState = Object.assign({}, state);

  switch(action.type) {

    case types.GET_TERM_SUCCESS:
      console.info('GET_TERM_SUCCESS', action.data);
      // Modify next state depending on the action and return it
      // return [
      //   ...state, action.data
      // ];
      return update(state, {
        records: {
          $set: action.data.data
        },
        status: {
          $set: action.data.response
        }
      });
      // return Object.assign([], state, action.data);

    case types.UPDATE_TERM_SUCCESS:
      console.info('term reducer update', action.data);
      return update(state, {
        records: {
          $set: action.data.data
        },
        status: {
          $set: action.data.response
        }
      });

    default:
      /* Return original state if no actions were consumed. */
      return state;
  }
}
