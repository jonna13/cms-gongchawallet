/**
 * Created by jedachas on 3/8/17.
 */
import * as types from '../constants/InventoryActionTypes';
import update from 'react-addons-update';

const initialState = {
  inventorys: {
    records: [],
    pageinfo: {
      pageSize: 0,
      totalPage: 0,
      sortColumnName: null,
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 20,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: null,
      selectedLocation: ''
    }
  },
  locations: [],
  products: [],
  selectedRecord: {},
}

module.exports = function(state = initialState, action) {
  switch (action.type) {
    case types.VIEW_INVENTORY_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data[0]
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        inventorys: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_INVENTORY_SUCCESS:
      return update(state, {
        inventorys: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        }
      });

    case types.GET_INVENTORY_LOCATION_SUCCESS:
      return update(state, {
        locations: { $set: action.data.data }
      });

    case types.GET_INVENTORY_PRODUCT_SUCCESS:
      return update(state, {
        products: { $set: action.data.data }
      });

    default:
      return state;

  }
}
