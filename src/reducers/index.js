/* Combine all available reducers to a single root reducer.
 *
 * CAUTION: When using the generators, this file is modified in some places.
 *          This is done via AST traversal - Some of your formatting may be lost
 *          in the process - no functionality should be broken though.
 *          This modifications only run once when the generator is invoked - if
 *          you edit them, they are not updated again.
 */
import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
/* Populated by react-webpack-redux:reducer */
const reducers = {
  profileState: require('../reducers/profileReducer.js'),
  aboutState: require('../reducers/aboutReducer.js'),
  termState: require('../reducers/termReducer.js'),
  authReducer: require('../reducers/authReducer.js'),
  dialogState: require('../reducers/dialogReducer.js'),
  loadingState: require('../reducers/loadingReducer.js'),
  productState: require('../reducers/productReducer.js'),
  locationState: require('../reducers/locationReducer.js'),
  categoryState: require('../reducers/categoryReducer.js'),
  subcategoryState: require('../reducers/subcategoryReducer.js'),
  cymState: require('../reducers/cymReducer.js'),
  cymcategoryState: require('../reducers/cymcategoryReducer.js'),
  drinkState: require('../reducers/drinkReducer.js'),
  packageState: require('../reducers/packageReducer.js'),
  postState: require('../reducers/postReducer.js'),
  settingState: require('../reducers/settingReducer.js'),
  inventoryState: require('../reducers/inventoryReducer.js'),
  accountState: require('../reducers/accountReducer.js'),
  routing
};
module.exports = combineReducers(reducers);
