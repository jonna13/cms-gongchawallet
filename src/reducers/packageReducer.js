/**
 * Created by jedachas on 2/23/17.
 *
 * Profile Reducer
 *
 */
import * as types from '../constants/PackageActionTypes';
import update from 'react-addons-update';

const initialState = {
  packages: {
    records: [],
    pageinfo: {
      pageSize: 0,
      totalPage: 0,
      sortColumnName: null,
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 20,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: null
    }
  },
  selectedRecord: {},
  drinks: [],
  products: []
};

module.exports = function(state = initialState, action) {

  switch(action.type) {

    case types.VIEW_PACKAGE_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        packages: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_PACKAGE_SUCCESS:
      return update(state, {
        packages: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        }
      });

    case types.GET_PACKAGE_DRINK_SUCCESS:
      return update(state, {
        drinks: {
          $set: action.data.data
        }
      });

    case types.GET_PACKAGE_PRODUCT_SUCCESS:
      return update(state, {
        products: {
          $set: action.data.data
        }
      });


    default:
      /* Return original state if no actions were consumed. */
      return state;
  }
}
