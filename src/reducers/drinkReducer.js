/**
 * Created by jedachas on 3/8/17.
 */
import * as types from '../constants/DrinkActionTypes';
import update from 'react-addons-update';

const initialState = {
  drinks: {
    records: [],
    pageinfo: {
      pageSize: 0,
      totalPage: 0,
      sortColumnName: null,
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 20,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: null
    }
  },
  products: [],
  selectedRecord: {},
}

module.exports = function(state = initialState, action) {
  switch (action.type) {
    case types.VIEW_DRINK_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        drinks: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_DRINK_SUCCESS:
      return update(state, {
        drinks: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        }
      });

    case types.GET_DRINKPRODUCT_SUCCESS:
      return update(state, {
        products: {
          $set: action.data.data
        }
      });

    case types.GET_DRINKPRODUCT_FAILED:
      return update(state, {
        products: {
          $set: []
        }
      });

    default:
      return state;

  }
}
