/**
 * Created by jonna on 1/5/18.
 *
 * Setting Reducer
 *
 */
import * as types from '../constants/SettingActionTypes';
import Config from '../config/base';
import update from 'react-addons-update';

const initialState = {
  records: [],
  status: ''
};

module.exports = function(state = initialState, action) {
  /* Keep the reducer clean - do not mutate the original state. */
  //let nextState = Object.assign({}, state);

  switch(action.type) {

    case types.GET_SETTING_SUCCESS:
      console.info('GET_SETTING_SUCCESS', action.data);
      // Modify next state depending on the action and return it
      // return [
      //   ...state, action.data
      // ];
      return update(state, {
        records: {
          $set: action.data.data
        },
        status: {
          $set: action.data.response
        }
      });
      // return Object.assign([], state, action.data);

    case types.UPDATE_SETTING_SUCCESS:
      console.info('setting reducer update', action.data);
      return update(state, {
        records: {
          $set: action.data.data
        },
        status: {
          $set: action.data.response
        }
      });

    default:
      /* Return original state if no actions were consumed. */
      return state;
  }
}
