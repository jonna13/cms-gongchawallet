/**
 * Created by jedachas on 2/23/17.
 *
 * Profile Reducer
 *
 */
import * as types from '../constants/CymActionTypes';
import update from 'react-addons-update';

const initialState = {
  cyms: {
    records: [],
    pageinfo: {
      pageSize: 0,
      totalPage: 0,
      sortColumnName: null,
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 20,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: null
    },
    category: []
  },
  selectedRecord: {}
};

module.exports = function(state = initialState, action) {

  switch(action.type) {

    case types.VIEW_CYM_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        cyms: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_CYM_SUCCESS:
      return update(state, {
        cyms: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        }
      });
    case types.GET_CYM_CATEGORY_SUCCESS:
      return update(state, {
        cyms: {
          category: {
            $set: action.data.data
          }
        }
      });

    default:
      /* Return original state if no actions were consumed. */
      return state;
  }
}
