'use strict';

import { blue400, red700, red100, red400, white, darkBlack} from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

// Settings configured here will be merged into the final config object.
// 124.106.33.31
// 192.168.254.9
// dev defaults
export default {
  BUILD_NAME: 'Gongcha Wallet',
  BUILD_VERSION: 'v1.0.0',
  MERCHANT_NAME: 'Gongcha PH',
  PROJECT_PATH: 'http://122.53.63.92/project/gongchawallet/',
  PROJECT_BASE: '/project/gongchawallet/dashboard/',
  GATEWAY_URL: 'api.php',
  UPLOAD_URL: 'dashboard/php/upload.php',
  MUITHEME: getMuiTheme({
    palette: {
      accent1Color: red100,
      accent2Color: red400,
      primary1Color: red700,
      primary2Color: red700,
      textColor: darkBlack,
      alternateTextColor: white,
    }
  }),
  IMAGE: {
    PATH: 'assets/images/',
    PROFILE: 'profile',
    PRODUCT: 'products',
    CATEGORY: 'category',
    SUBCATEGORY: 'subcategory',
    LOCATION: 'locations',
    LOYALTY: 'loyalty',
    VOUCHER: 'vouchers',
    POST: 'posts',
    SETTING: 'settings',
    CASHIER: 'cashiers',
    FLAVOR: 'flavors',
    DRINK: 'drinks',
    SIDES: 'sides',
    ACCOUNT: 'accounts',
    PACKAGE: 'package'
  },
  GATEWAY: {
    CATEGORY: 'dashboard',
  },
  DIALOG_MESSAGE: {
    CONFIRM_TITLE: 'Validate',
    UPDATE_PROFILE_MESSAGE: 'Are you sure you want to update company profile?',
    ADD_PRODUCT_MESSAGE: 'Are you sure you want to add new product?',
    UPDATE_PRODUCT_MESSAGE: 'Are you sure you want to update selected product?',
    ADD_CATEGORY_MESSAGE: 'Are you sure you want to add new category?',
    UPDATE_CATEGORY_MESSAGE: 'Are you sure you want to update selected category?',
    ADD_SUBCATEGORY_MESSAGE: 'Are you sure you want to add new subcategory?',
    UPDATE_SUBCATEGORY_MESSAGE: 'Are you sure you want to update selected subcategory?',
    ADD_LOCATION_MESSAGE: 'Are you sure you want to add new branch?',
    UPDATE_LOCATION_MESSAGE: 'Are you sure you want to update selected branch?',
    ADD_SETTING_MESSAGE: 'Are you sure you want to add new setting?',
    UPDATE_SETTING_MESSAGE: 'Are you sure you want to update selected setting?',
    ADD_DRINK_MESSAGE: 'Are you sure you want to add new drink?',
    UPDATE_DRINK_MESSAGE: 'Are you sure you want to update selected drink?',
    ADD_INVENTORY_MESSAGE: 'Are you sure you want to add new inventory?',
    UPDATE_INVENTORY_MESSAGE: 'Are you sure you want to update selected inventory?',
    UPDATE_ABOUT_MESSAGE: 'Are you sure you want to update about us?',
    UPDATE_CREDENTIAL_MESSAGE: 'Are you sure you want to update credentials?',
    ADD_ACCOUNT_MESSAGE: 'Are you sure you want to add new account?',
    UPDATE_ACCOUNT_MESSAGE: 'Are you sure you want to update selected account?',
    ADD_CYMCATEGORY_MESSAGE: 'Are you sure you want to add new cym category?',
    UPDATE_CYMCATEGORY_MESSAGE: 'Are you sure you want to update selected cym category?',
    ADD_CYM_MESSAGE: 'Are you sure you want to add new cym?',
    UPDATE_CYM_MESSAGE: 'Are you sure you want to update selected cym?',
    ADD_PACKAGE_MESSAGE: 'Are you sure you want to add new package?',
    UPDATE_PACKAGE_MESSAGE: 'Are you sure you want to update selected package?',
    ADD_POST_MESSAGE: 'Are you sure you want to add new exclusives?',
    UPDATE_POST_MESSAGE: 'Are you sure you want to update selected exclusives?',
    CONFIRM_LABEL: 'Yes',
    CLOSE_LABEL: 'No',
    CANCEL_LABEL: 'Cancel',
    NOTIFICATION_DELAY: 5000,
  },
  ERROR_MESSAGE: {
    ERROR_TITLE: 'Error',
    ERROR_DESCRIPTION: 'Oops! Something went wrong. Kindly check the internet.',
    EXPIRED_TITLE: 'Expired',
    EXPIRED_DESCRIPTION: 'Oops! It seems your session expired',
    EMPTY_TITLE: 'Empty',
    EMPTY_DESCRIPTION: 'Oops! No record found',
    FAILED_TITLE: 'Failed',
    FAILED_FETCH_DESCRIPTION: 'Oops! It seems that fetching data from the server failed. Kindly check your internet connection and reload the page',
    FAILED_ADD_DESCRIPTION: 'Oops! It seems there\'s a problem adding a new record. Kindly check your internet connection and reload the page',
    FAILED_UPDATE_DESCRIPTION: 'Oops! It seems there\'s a problem updating your record. Kindly check your internet connection and reload the page',
    CONNECTION_TITLE: 'Connection Error',
    CONNECTION_DESCRIPTION: 'Oops! Unable to connect to server. Please check your internet connection and reload the page',
  },
  ERROR_CODE: {
    PROFILETABLE: '#200',
    PRODUCTTABLE: '#201',
    PRODCATEGORYTABLE: '#202',
    PRODSUBCATEGORYTABLE: '#203',
    LOCATIONTABLE: '#204',
    LOYALTYTABLE: '#205',
    VOUCHERTABLE: '#206',
    POSTTABLE: '#207',
    TABLETTABLE: '#208',
    SETTINGTABLE: '#209',
    CASHIERTABLE: '#210',
    DRINKTABLE: '#211',
    ADDONTABLE: '#212',
    FLAVORTABLE: '#213',
    DIPTABLE: '#214',
    SIDETABLE: '#215',
    UPGRADETABLE: '#216',
    INVENTORYTABLE: '#217',
    ACCOUNTTABLE: '#218',
    TRANSACTIONTABLE: '#219',
    CYMTABLE: '#310',
    CYMCATEGORYTABLE: '#311',
    PACKAGETABLE: '#312',
    ABOUTTABLE: '#313',
  },
  IMAGE_SIZE_LIMIT: 2e+6,
  SUPERUSER: ['developer'],
  FIELD_EDIT:{
    PRIMARY_MAX_SIZE: 500,
    SECONDARY_MAX_SIZE: 1000,
    TERTIARY_MAX_SIZE: 1500
  },
  TEAM_CODE: 'CATzX21143XIADgf',
  SOLO_CODE: 'CATkL21257DJBEjt',
  DRINK_CODE: 'SUBoE144544WyNbn',
  SIDES_CODE: 'SUBA8184650KauNm',
  UNIT_INCLUDES: ['CATx221411pMu1qX', 'CATjy21514HIg3mq'], // drink, sides
  GODUSER: 'developer',
  SUPERUSER: ['developer', 'project-manager'],
  ACCOUNT_ROLES: [
    {id: 1, name: 'Project Manager', value: 'project-manager'},
    {id: 2, name: 'Administrator', value: 'administrator'},
    {id: 3, name: 'Marketing', value: 'marketing'},
    {id: 4, name: 'Manager', value: 'manager'},
    {id: 5, name: 'Staff', value: 'staff'},
    {id: 7, name: 'Store Operation', value: 'store-operation'}
  ],
  DEVELOPER_ROLE: [{id: 6, name: 'Developer', value: 'developer'}],
  ACCOUNT_ROLE_TABS: {
    'developer' : {
      reports: ['all'],
      cms: ['all']
    },
    'project-manager' : {
      reports: ['all'],
      cms: ['all']
    },
    'administrator': {
      reports: ['all'],
      cms: ['profile', 'push', 'category', 'subcategory', 'product', 'flavor', 'drink', 'side', 'post', 'setting']
    },
    'marketing': {
      reports: ['all'],
      cms: ['profile','post','product']
    },
    'manager': {
      reports: [],
      cms: ['profile', 'push', 'category', 'subcategory', 'product', 'flavor', 'drink', 'side', 'post']
    },
    'store-operation': {
      reports: [],
      cms: ['order', 'inventory']
    }
  },
  CUSTOM_ROLE: 'staff',
  CMS_TABS: [
    {id: 1, name: 'Profile', value: 'profile'},
    {id: 2, name: 'Push', value: 'push'},
    {id: 3, name: 'Main Category', value: 'category'},
    {id: 4, name: 'Sub category', value: 'subcategory'},
    {id: 5, name: 'Product', value: 'product'},
    {id: 6, name: 'Location', value: 'location'},
    {id: 7, name: 'Flavor', value: 'flavor'},
    {id: 8, name: 'Account', value: 'account'},
    {id: 9, name: 'Order', value: 'order'},
    {id: 10, name: 'Drink', value: 'drink'},
    {id: 11, name: 'Side', value: 'side'},
    {id: 12, name: 'Inventory', value: 'inventory'},
    {id: 13, name: 'Setting', value: 'setting'},
    {id: 999, name: 'All', value: 'all'}
  ],
  REPORT_TABS: [
    {id: 1, name: 'Downloads', value: 'downloads'},
    {id: 2, name: 'Card Registration', value: 'card-registration'},
    {id: 3, name: 'Demographics', value: 'demographics'},
    {id: 4, name: 'Loyalty Sales', value: 'loyalty'},
    {id: 5, name: 'Vouchers', value: 'vouchers'},
    {id: 6, name: 'Points Redemption', value: 'points-redemption'},
    {id: 999, name: 'All', value: 'all'}
  ]
  // GATEWAY_URL: 'http://savenearn.appsolutely.ph/dashboard/php/gateway.test.php'
}
