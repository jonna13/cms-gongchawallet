import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/inventory/InventoryComponent';

class Inventory extends Component {
  render() {
    const {actions, inventoryState, router} = this.props;
    return <Main actions={actions} data={inventoryState} router={router}/>;
  }
}

Inventory.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { inventoryState: state.inventoryState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = { inventoryAction : require('../actions/inventoryAction.js') };
  const actionMap = { actions: bindActionCreators(actions.inventoryAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Inventory);
