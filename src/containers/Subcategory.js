import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/subcategory/SubcategoryComponent';

class Subcategory extends Component {
  render() {
    const {actions, subcategoryState, router} = this.props;
    return <Main actions={actions} data={subcategoryState} router={router}/>;
  }
}

Subcategory.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { subcategoryState: state.subcategoryState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    subcategoryAction: require('../actions/subcategoryAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.subcategoryAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Subcategory);
