import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/package/PackageComponent';

class Package extends Component {
  render() {
    const {actions, packageState, router} = this.props;
    return <Main actions={actions} data={packageState} router={router}/>;
  }
}

Package.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { packageState: state.packageState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = { packageAction : require('../actions/packageAction.js') };
  const actionMap = { actions: bindActionCreators(actions.packageAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Package);
