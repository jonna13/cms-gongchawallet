import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/cymcategory/CymCategoryComponent';

class CymCategory extends Component {
  render() {
    const {actions, cymcategoryState, router } = this.props;
    return <Main actions={actions} data={cymcategoryState} router={router}/>;
  }
}

CymCategory.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { cymcategoryState: state.cymcategoryState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    cymcategoryAction: require('../actions/cymcategoryAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.cymcategoryAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(CymCategory);
