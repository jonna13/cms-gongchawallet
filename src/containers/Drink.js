import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/drink/DrinkComponent';

class Drink extends Component {
  render() {
    const {actions, drinkState, router} = this.props;
    return <Main actions={actions} data={drinkState} router={router}/>;
  }
}

Drink.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { drinkState: state.drinkState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = { drinkAction : require('../actions/drinkAction.js') };
  const actionMap = { actions: bindActionCreators(actions.drinkAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Drink);
