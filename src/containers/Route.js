import React from 'react';
import { Route, IndexRoute } from 'react-router';

import Main from '../components/Main';
import App from './App';
import Login from './Login';
import Push from './Push';
import Product from './Product';
import Category from './Category';
import CymCategory from './CymCategory';
import Cym from './Cym';
import Subcategory from './Subcategory';
import Location from './Location';
import Setting from './Setting';
import About from './About';
import Term from './Term';
import Account from './Account';
import Inventory from './Inventory';
import Package from './Package';
import Drink from './Drink';
import Post from './Post';

import ProductEditor from '../components/product/ProductEditor';
import CategoryEditor from '../components/category/CategoryEditor';
import CymCategoryEditor from '../components/cymcategory/CymCategoryEditor';
import CymEditor from '../components/cym/CymEditor';
import SubcategoryEditor from '../components/subcategory/SubcategoryEditor';
import LocationEditor from '../components/location/LocationEditor';
import SettingEditor from '../components/setting/SettingEditor';
import ProfileEditor from '../components/profile/ProfileEditor';
import AboutEditor from '../components/about/AboutEditor';
import TermEditor from '../components/term/TermEditor';
import CredentialEditor from '../components/auth/CredentialEditor';
import AccountEditor from '../components/account/AccountEditor';
import InventoryEditor from '../components/inventory/InventoryEditor';
import PackageEditor from '../components/package/PackageEditor';
import DrinkEditor from '../components/drink/DrinkEditor';
import PostEditor from '../components/post/PostEditor';

import Config from '../config/base';

const requireAuth = (nextState, replace) => {
  if (!sessionStorage.getItem(Config.MERCHANT_NAME)) {
    replace({
      pathname: '/login',
      state: { nextPathname: nextState.location.pathname }
    });
  }
}


module.exports = (
    <div>
      <Route path="/" component={Main} > {/* Holds the Navigation links */}
        <IndexRoute component={App} onEnter={requireAuth}/> {/* Landing Page w/c is Profile Page */}
        <Route path="/profile_edit" component={ProfileEditor} onEnter={requireAuth} />
        <Route path="/about" component={About} onEnter={requireAuth} />
        <Route path="/about_edit" component={AboutEditor} onEnter={requireAuth} />
        <Route path="/term" component={Term} onEnter={requireAuth} />
        <Route path="/term_edit" component={TermEditor} onEnter={requireAuth} />
        <Route path="/setting" component={Setting} onEnter={requireAuth} />
        <Route path="/setting_edit" component={SettingEditor} onEnter={requireAuth} />
        <Route path="/credential" component={CredentialEditor} onEnter={requireAuth} />
       {/*<Route path="/setting" component={Setting} onEnter={requireAuth} />
        <Route path="/setting/:id" component={SettingEditor} onEnter={requireAuth}/>
        <Route path="/setting_add" component={SettingEditor} onEnter={requireAuth}/>*/}
        <Route path="/push" component={Push} onEnter={requireAuth}/>
        <Route path="/product" component={Product} onEnter={requireAuth} />
        <Route path="/product/:id(/:catid)" component={ProductEditor} onEnter={requireAuth}/>
        <Route path="/product_add" component={ProductEditor} onEnter={requireAuth}/>
        <Route path="/category" component={Category} onEnter={requireAuth} />
        <Route path="/category/:id" component={CategoryEditor} onEnter={requireAuth} />
        <Route path="/category_add" component={CategoryEditor} onEnter={requireAuth} />
        <Route path="/subcategory" component={Subcategory} onEnter={requireAuth} />
        <Route path="/subcategory/:id" component={SubcategoryEditor} onEnter={requireAuth} />
        <Route path="/subcategory_add" component={SubcategoryEditor} onEnter={requireAuth} />
        <Route path="/cymcategory" component={CymCategory} onEnter={requireAuth} />
        <Route path="/cymcategory/:id" component={CymCategoryEditor} onEnter={requireAuth} />
        <Route path="/cymcategory_add" component={CymCategoryEditor} onEnter={requireAuth} />
        <Route path="/cym" component={Cym} onEnter={requireAuth} />
        <Route path="/cym/:id" component={CymEditor} onEnter={requireAuth} />
        <Route path="/cym_add" component={CymEditor} onEnter={requireAuth} />
        <Route path="/location" component={Location} onEnter={requireAuth} />
        <Route path="/location/:id" component={LocationEditor} onEnter={requireAuth} />
        <Route path="/location_add" component={LocationEditor} onEnter={requireAuth} />
        <Route path="/inventory" component={Inventory} onEnter={requireAuth} />
        <Route path="/inventory/:id" component={InventoryEditor} onEnter={requireAuth} />
        <Route path="/inventory_add" component={InventoryEditor} onEnter={requireAuth} />
        <Route path="/account" component={Account} onEnter={requireAuth} />
        <Route path="/account/:id" component={AccountEditor} onEnter={requireAuth} />
        <Route path="/account_add" component={AccountEditor} onEnter={requireAuth} />
        <Route path="/package" component={Package} onEnter={requireAuth} />
        <Route path="/package/:id" component={PackageEditor} onEnter={requireAuth} />
        <Route path="/package_add" component={PackageEditor} onEnter={requireAuth} />
        <Route path="/drink" component={Drink} onEnter={requireAuth} />
        <Route path="/drink/:id" component={DrinkEditor} onEnter={requireAuth} />
        <Route path="/drink_add" component={DrinkEditor} onEnter={requireAuth} />
        <Route path="/post" component={Post} onEnter={requireAuth}/>
        <Route path="/post/:id" component={PostEditor} onEnter={requireAuth}/>
        <Route path="/post_add" component={PostEditor} onEnter={requireAuth}/>
      </Route>
      <Route path="/login" component={Login} />
    </div>
);
