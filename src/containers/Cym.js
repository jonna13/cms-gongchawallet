import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/cym/CymComponent';

class Cym extends Component {
  render() {
    const {actions, cymState, router } = this.props;
    return <Main actions={actions} data={cymState} router={router}/>;
  }
}

Cym.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { cymState: state.cymState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    cymAction: require('../actions/cymAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.cymAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Cym);
