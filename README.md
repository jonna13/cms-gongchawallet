# Introduction
Repository project of Bonchon CMS which includes:

- Profile Module

- Settings Module

- Push Module

- Product Category Module

- Product Subcategory Module

- Product Module

- Dip Module

- Addon Module



# How to run
type `npm i`.
then run `npm run serve`.

# How to deploy
type `npm run dist`.